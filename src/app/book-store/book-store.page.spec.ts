import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BookStorePage } from './book-store.page';

describe('BookStorePage', () => {
  let component: BookStorePage;
  let fixture: ComponentFixture<BookStorePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookStorePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BookStorePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
