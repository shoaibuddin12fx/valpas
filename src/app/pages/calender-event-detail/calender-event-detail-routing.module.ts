import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CalenderEventDetailPage } from './calender-event-detail.page';

const routes: Routes = [
  {
    path: '',
    component: CalenderEventDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CalenderEventDetailPageRoutingModule {}
