import { AfterViewInit, asNativeElements, Component, ElementRef, Injector, LOCALE_ID, OnInit, ViewChild } from '@angular/core';
import { BobjectsService } from 'src/app/services/bobjects.service';
import { BasePage } from '../base-page/base-page';
import { CalendarComponent } from 'ionic2-calendar/calendar';
import { CalendarService } from 'src/app/services/calendar.service';

@Component({
  selector: 'app-calender',
  templateUrl: './calender.page.html',
  styleUrls: ['./calender.page.scss'],
})
export class CalenderPage extends BasePage implements OnInit, AfterViewInit {

  viewTitle: string;
  type = 'week';  
  loading;
  orders;
  calendar = {
    mode: this.type,
    currentDate: new Date(),
    locale: this.translation.lang
  };
 
  selectedDate: Date;
 
  @ViewChild('myCal', {static: true}) myCal:CalendarComponent;

  constructor(injector: Injector, public calendarService: CalendarService) { 
    super(injector);
    this.initialize();
    this.events.subscribe("update:calendar", this.changeCalLang.bind(this))
    console.log("again and again +2")
  }

  ionViewDidEnter(){
    console.log("again and again +3")
    if(this.type == 'week'){
      this.calendar.mode = this.type = 'month'
    }

    if(this.type == 'month'){
      this.calendar.mode = this.type = 'week'
    }

    if(this.type == 'day'){
      this.calendar.mode = this.type = 'month'
    }

    let lang = this.translation.lang;

    this.translation.getTranslateKey('week').then( wt => {
      let t = this.viewTitle.replace('Week', 'week').replace('VKO', 'week').replace('vecka', 'week').replace('week', wt);
      this.viewTitle = t;
    })
    

  }

  ngAfterViewInit(): void {
    this.calendar.locale = this.translation.lang;
    console.log(this.calendar.locale);
    console.log("again and again +1")
  }

  ngOnInit(): void {
    console.log("again and again")
  }

  ionViewWillEnter(){
    
  }

  async initialize(){
    
  }

  async changeCalLang(key){
    this.calendar.locale = key;
    this.myCal.loadEvents()
  }

  // Change current month/week/day
  async next() {
    await this.platform.ready();
    this.myCal.slideNext();

  }

  async onRangeChanged($event){
    console.log($event);
    let start =  $event.startTime;
    let end = $event.endTime;

    console.log(start,end);
    this.loading = true;
    await this.calendarService.initializeData(this.calendarService.getfirstLastRange(start, end))
    // let data:any = this.calendarService.network.getAllMyWorks();
    this.orders= this.calendarService.eventSource;
  console.log(this.orders)
    this.loading = false;
  }
 
  async back() {
    await this.platform.ready();
    this.myCal.slidePrev();
  }
 
  // Selected date reange and hence title changed
  async onViewTitleChanged(title) {

    // manipulatin of string 
    this.translation.getTranslateKey('week').then( wt => {
      if(title){
        let t = title.replace('Week', 'week').replace('VKO', 'week').replace('vecka', 'week').replace('week', wt);
        this.viewTitle = t;
      }
      
    })
  }
 
  // Calendar event was clicked
  async onEventSelected(event) {
    // Use Angular date pipe for conversion
    localStorage.setItem('witem', JSON.stringify(event.data));
    const data = this.nav.push('works-detail')

  }
 
    
 
  removeEvents() {
    
  }

}
