import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-monthview',
  templateUrl: './monthview.component.html',
  styleUrls: ['./monthview.component.scss'],
})
export class MonthviewComponent implements OnInit {

  @Input('text') text = "";
  constructor() { }

  ngOnInit() {}

}
