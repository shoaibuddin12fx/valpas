import { Component, Injector, OnInit } from '@angular/core';
import { BobjectsService } from 'src/app/services/bobjects.service';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.page.html',
  styleUrls: ['./customers.page.scss'],
})
export class CustomersPage extends BasePage implements OnInit {

  plist = [];
  storedData = {
    search: "",
    offset: 0
  }

  constructor(injector: Injector, public bobject: BobjectsService) { 
    super(injector);
    
  }

  ngOnInit() {
    this.initialize();
  }

  async initialize(){
    const data = await this.getCustomerList();
    console.log(data)
  }

  getCustomerList(): Promise<any>{

    return new Promise( resolve => {

      if(this.storedData.offset == -1){
        resolve(true);
      }else{
        this.sqlite.getBuildingsItems(this.storedData.search, this.storedData.offset).then((res: any) => {

          console.log("of", this.storedData.offset);
          if(this.storedData.offset == 0){
            this.plist = [];
            this.plist = res['buildings'];
            console.log(this.plist);
          }else{
            this.plist = this.plist.concat(res['buildings']);
          }

          this.storedData.offset = res['offset'];
          resolve(true);

        }, err => {})
      }


    })


    

  }

  openWorks(item){
    console.log(item);
    // item.buildingId = 7;
    localStorage.setItem("customerBuildingId", item.buildingId);
    localStorage.setItem("customer", JSON.stringify(item));
    this.nav.push(`works/c${item.id}`, {customer: true, item: JSON.stringify(item)})
  }

  ionChange($event){
    let v = $event.target.value;
    console.log(v);
    this.storedData.offset = 0;
    this.storedData.search = v;
    this.getCustomerList();
  }

  loadMore($event){

    this.getCustomerList().then( v => {
      $event.target.complete();
    });

  }

}
