import { Component } from '@angular/core';
import { NavController, LoadingController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { CheckBoxService } from './../../services/check-box-service';
import { ToastService } from 'src/app/services/toast-service';
import { FormControl } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';
import { LoadingService } from 'src/app/services/loading-service';
import { Subject } from 'rxjs';
import { CommonService } from 'src/app/services/common.service';

@Component({
    templateUrl: 'item-details-check-box.page.html',
    styleUrls: ['item-details-check-box.page.scss'],
    providers: [CheckBoxService]
})
export class ItemDetailsCheckBoxPage {

    data: any;
    tempData: any;
    type: string;
    searchControl: FormControl;
    searching: any = false;
    itemClicked: boolean = false;
    tempArray:any = [];
    loading: any;
    
    
    constructor(
        public navCtrl: NavController,
        private service: CheckBoxService,
        private toastCtrl: ToastService,
        public loadingController: LoadingService,
        private route: ActivatedRoute,
        private router: Router,
        private commonService: CommonService) {
        this.type = this.route.snapshot.paramMap.get('type');
        this.getShipmentDetails();
        this.searchControl = new FormControl();        
    }

    isType(item) {
        return item === parseInt(this.type, 10);
    }

    // events
    onItemClick(params): void {
        if(params.isChecked){
            this.tempArray.push(params.id);
        } else {
            this.tempArray.splice(this.tempArray.indexOf(params.id), 1);
        }

        if(this.tempArray.length>0){
            this.itemClicked = true;
        } else {
            this.itemClicked = false;
        }
        
    }

    bookNow(){
        this.commonService.addShipmentData(this.tempArray);
        this.router.navigateByUrl("book-store");
    }

    async getShipmentDetails(){
        this.loadingController.show();
        this.service.getShippingDetails().subscribe(d =>{
            this.loadingController.hide();
            this.data = d.responseData.entities;
            this.tempData = this.data;
        },error =>{
            console.log('failure'+error);
            this.loadingController.hide();
        });
    }

    onSearchTerm(searchTerm) {
        let searchVal = searchTerm.detail.value;
        this.data = this.tempData;

        if (searchVal && searchVal.trim() !== '') {
        
            this.data = this.data.filter(item => {
                return item.shipmentNo.indexOf(searchVal) > -1;
            });
        }
    }
}
