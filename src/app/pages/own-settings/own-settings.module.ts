import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OwnSettingsPageRoutingModule } from './own-settings-routing.module';

import { OwnSettingsPage } from './own-settings.page';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TranslateModule,
    OwnSettingsPageRoutingModule
  ],
  declarations: [OwnSettingsPage]
})
export class OwnSettingsPageModule {}
