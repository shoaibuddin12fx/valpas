import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';
import { FormGroup, Validators } from '@angular/forms';
const environment = require('./../../../environments/environment')
@Component({
  selector: 'app-own-settings',
  templateUrl: './own-settings.page.html',
  styleUrls: ['./own-settings.page.scss'],
})
export class OwnSettingsPage extends BasePage implements OnInit {

  isProduction = false;
  localItems: any[];
  languages: any[] = [];
  menus: any[] = [];
  aform: FormGroup;
  constructor(injector: Injector) { 
    super(injector)
    this.languages = this.translation.getTranslationsList();
    // this.isProduction = !environment.SERVER_URL.includes('test');
    // console.log(this.isProduction)
    this.initializeLocalitems();
    this.initializeSidemenuOptions()
  }

  ngOnInit() {
  }

  async initializeLocalitems(){

    this.localItems = [
      {
        title: 'meters',
        toggle: false
      }

    ];

    for(var i = 0; i < this.localItems.length; i++){
      this.localItems[i].toggle = localStorage.getItem(this.localItems[i].title);
    }
  }

  async initializeSidemenuOptions(){
    this.menus = await this.sqlite.getUserSidemenu() as [];
  }

  onChangeLang(event: CustomEvent) {

    if (!event) return;

    const lang = event.detail.value;

    if (lang === 'ar') {
      document.dir = 'rtl';
    } else {
      document.dir = 'ltr';
    }

    this.translation.useTranslation(lang);
   
  }

  async toggleSidemenuItem(menu){
    await this.sqlite.setSidemenuItem(menu);
    this.events.publish('user:init_sidemenu');
  }

  async toggleStorageItem(menu){    
    localStorage.setItem(menu.title, menu.toggle );
  }

  async showLogs(){
    this.nav.push('logs');
  }



}
