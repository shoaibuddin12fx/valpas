import { AfterViewInit, Component, Injector, Input, OnInit } from '@angular/core';
import { BasePage } from '../../base-page/base-page';

@Component({
  selector: 'app-entities',
  templateUrl: './entities.component.html',
  styleUrls: ['./entities.component.scss'],
})
export class EntitiesComponent extends BasePage implements OnInit, AfterViewInit {

  @Input() entities;
  selectedEntity;
  filterTerm;
  constructor(injector: Injector) { 
    super(injector);
    
  }
  
  ngOnInit() {}

  ngAfterViewInit(): void {
    
  }

  selectEntity(item){
    this.back(item);
  }

  search($event){
    
  }

  back(res){
    this.modals.dismiss(res);
  }






}
