import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { QrInPage } from './qr-in.page';

const routes: Routes = [
  {
    path: '',
    component: QrInPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class QrInPageRoutingModule {}
