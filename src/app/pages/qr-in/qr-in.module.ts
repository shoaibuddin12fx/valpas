import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { QrInPageRoutingModule } from './qr-in-routing.module';
import { QrInPage } from './qr-in.page';
import { TranslateModule } from '@ngx-translate/core';
import { EntitiesComponent } from './entities/entities.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    QrInPageRoutingModule,
    TranslateModule,
    Ng2SearchPipeModule
  ],
  exports: [
    EntitiesComponent
  ],
  declarations: [QrInPage, EntitiesComponent ]
})
export class QrInPageModule {}
