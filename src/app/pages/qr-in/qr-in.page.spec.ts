import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { QrInPage } from './qr-in.page';

describe('QrInPage', () => {
  let component: QrInPage;
  let fixture: ComponentFixture<QrInPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QrInPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(QrInPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
