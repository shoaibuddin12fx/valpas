import { AfterViewInit, Component, Injector, OnInit } from '@angular/core';
import { resolve } from 'path';
import { BobjectsService } from 'src/app/services/bobjects.service';
import { BasePage } from '../base-page/base-page';
import { EntitiesComponent } from './entities/entities.component';

@Component({
  selector: 'app-qr-in',
  templateUrl: './qr-in.page.html',
  styleUrls: ['./qr-in.page.scss'],
})
export class QrInPage extends BasePage implements OnInit, AfterViewInit {

  project_id;
  projectData;
  user;
  entities = [];
  entity;
  description;
  km_billable = 0;
  km_work = 0;
  daily_allowence = 0;
  lunch_allowence = 0;
  hideAllowence = true;
  isScanSuccess = false;
  inTime;




  constructor(injector: Injector, public bobject: BobjectsService) { 
    super(injector);
    
  }
  
  ngOnInit() {

  }

  ngAfterViewInit(): void {
    this.initialize();
    this.getLitteras();
  }

  async initialize(): Promise<any> {

    return new Promise(async resolve => {

      this.user = await this.users.getUserData();
      this.project_id = this.getQueryParams().project_id;
      if(this.project_id){
        this.projectData = JSON.parse(this.getQueryParams().project);
        this.inTime = this.projectData['inTime'];

        

      }else{
        this.nav.pop();
      }
    

      resolve()
    })

  }

  async getLitteras(): Promise<any>{
    return new Promise( async resolve => {

      const bobj = await this.bobject.prepareObject();
      const obj = {
        "ownerId": parseInt(this.project_id)
      };

      const merged = {...bobj, ...obj}

      this.network.getAllLitteras(merged).then( async v => {
        this.entities = v['entities'];
        this.entity = this.entities.length > 0 ? this.entities[0] : null;
        if(this.inTime){
          this.entity = await this.storage.getKey('qrin_entity');
        }
        resolve();
      })

      
    })
  }

  async selectEntity(){
    const _data = await this.modals.present(EntitiesComponent, {entities : this.entities})
    const data = _data.data;
    if(data['data'] != 'A'){
      this.entity = data;
    }

  }

  async qrcodeIn(){
    const obj = await this.bobject.prepareObject();
    const data = await this.callNetworkResponse(obj, true);
    this.nav.pop();
  }

  async qrcodeOut() {
    const obj = await this.bobject.prepareObject();
    const data = await this.callNetworkResponse(obj, false);
    if(data){
      this.nav.pop();
    }
    
  }

  callNetworkResponse(obj, flag): Promise<any>{

    return new Promise( resolve => {

      if(!this.entity){
        resolve(); return;
      }

      const _data = {
        "start":flag,
        "freeText":this.description,
        "vehicleCode":null,
        "kmBillable":this.km_billable,
        "kmWork":this.km_work,
        "amount":0,
        "lunchAllowance":this.lunch_allowence,
        "dailyAllowance":this.daily_allowence,
        "litteraId":this.entity.id,
      }

      if(flag == true){
        this.storage.setKey('qrin_entity', this.entity);
      }

      var merged = {..._data, ...obj}

      merged["latitude"] = this.projectData["latitude"];
      merged["longitude"] = this.projectData["longitude"];

      
      this.network.getItsQrcode(merged).then( res => {
        this.isScanSuccess = true;
        
        this.events.publish("projects:loadall");
        resolve(this.projectData);
  
      }, err => {
        this.isScanSuccess = false;
        resolve(null)
        
      })
    })
    
  }



}
