import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SiteDiaryPage } from './site-diary.page';

const routes: Routes = [
  {
    path: '',
    component: SiteDiaryPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SiteDiaryPageRoutingModule {}
