import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SiteDiaryPageRoutingModule } from './site-diary-routing.module';

import { SiteDiaryPage } from './site-diary.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SiteDiaryPageRoutingModule
  ],
  declarations: [SiteDiaryPage]
})
export class SiteDiaryPageModule {}
