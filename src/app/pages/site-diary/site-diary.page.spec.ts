import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SiteDiaryPage } from './site-diary.page';

describe('SiteDiaryPage', () => {
  let component: SiteDiaryPage;
  let fixture: ComponentFixture<SiteDiaryPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SiteDiaryPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SiteDiaryPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
