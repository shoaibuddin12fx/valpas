import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TimeNotesPage } from './time-notes.page';

const routes: Routes = [
  {
    path: '',
    component: TimeNotesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TimeNotesPageRoutingModule {}
