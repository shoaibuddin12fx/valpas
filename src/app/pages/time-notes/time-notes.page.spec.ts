import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TimeNotesPage } from './time-notes.page';

describe('TimeNotesPage', () => {
  let component: TimeNotesPage;
  let fixture: ComponentFixture<TimeNotesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TimeNotesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TimeNotesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
