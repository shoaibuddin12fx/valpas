import { Component, Injector, OnInit } from '@angular/core';
import { BobjectsService } from 'src/app/services/bobjects.service';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-time-notes',
  templateUrl: './time-notes.page.html',
  styleUrls: ['./time-notes.page.scss'],
})
export class TimeNotesPage extends BasePage implements OnInit {

  projects: any[] = [];
  filterTerm;

  loading = false;
  constructor(injector: Injector, public bobjectsService: BobjectsService) { 
    super(injector)
    this.initialize();
    this.events.unsubscribe("projects:loadall");
    this.events.subscribe("projects:loadall", this.initialize.bind(this));
  }



  ngOnInit() {
    
  }

  ionViewWillEnter() {

  }

  async initialize(){
    
    
    if(this.loading == true){
      return;
    }
    
    // this.loading = true;
    const bobj = await this.bobjectsService.prepareObject()
    const merged = {...bobj}
    console.log(merged)
    
    this.network.getAllProjects(merged).then( v => {
      console.log(v);

      this.projects = v.projects.map(p => {
        var o = p;
        o['completeAddress'] = this.compileAddress(o.address);
        return o;
      });

      console.log(this.projects);
      // this.loading = false;
    })
  }

  compileAddress(obj: any): string {

    function addSpace(str){
      return str ? str + ' ' : '';
    }

    var str = '';
    str += addSpace(obj.streetName1);
    str += addSpace(obj.streetName2);
    str += addSpace(obj.entityName);
    str += addSpace(obj.city);
    str += addSpace(obj.country);
    str += addSpace(obj.postCode);
    obj.completeAddress = str;
    
    return str;

  }

  openProjectQrcode(item){
    this.nav.push('qr_in', {project_id: item.id, project: JSON.stringify(item)})
  }



}
