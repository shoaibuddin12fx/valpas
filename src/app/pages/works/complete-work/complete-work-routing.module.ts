import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CompleteWorkPage } from './complete-work.page';

const routes: Routes = [
  {
    path: '',
    component: CompleteWorkPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CompleteWorkPageRoutingModule {}
