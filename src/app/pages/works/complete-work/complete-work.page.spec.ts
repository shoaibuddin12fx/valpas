import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CompleteWorkPage } from './complete-work.page';

describe('CompleteWorkPage', () => {
  let component: CompleteWorkPage;
  let fixture: ComponentFixture<CompleteWorkPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompleteWorkPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CompleteWorkPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
