import { AfterViewInit, Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../../base-page/base-page';
import { WorkClassesComponent } from './work-classes/work-classes.component';
import { WorkCommentComponent } from './work-comment/work-comment.component';

@Component({
  selector: 'app-complete-work',
  templateUrl: './complete-work.page.html',
  styleUrls: ['./complete-work.page.scss'],
})
export class CompleteWorkPage extends BasePage implements OnInit, AfterViewInit {

  item: any;
  plist = []; 
  orderLines = [];
  commentSubmitted = false;
  entity = {
    description:"",
    subOrderReferences:null,
    orderId:0,
    orderTypeId: null,
    completeType:3,
    hoursSpent:null
  }

  selectedWork = [];
  commentSpinner = false;

  constructor(injector: Injector) { 
    super(injector);
    this.getAllWorkList();
  }
  ngAfterViewInit(): void {
    this.item = JSON.parse(localStorage.getItem('witem'));
    this.orderLines = JSON.parse(localStorage.getItem('plist'));
    this.entity.orderId = this.item.orderId;
  }

  ngOnInit() {
  }

  async getAllWorkList(){
    let data = await this.works.getOrderTypesByOrderId(this.entity) as []
    console.log(data);
    this.plist = data;
    this.selectedWork[0] = this.plist[0];
    this.entity.orderTypeId = this.selectedWork[0]['id']
  }

  async save($event){
    console.log(this.entity);
    let flag = await this.works.setOrderComplete(this.entity);
    this.translation.getTranslateKey('info_data_update_success').then( v => {
      this.utility.presentSuccessToast(v)
    })
    this.nav.pop().then( () => {
      this.nav.pop();
    })

  }

  back(){
    this.nav.pop();
    this.modals.dismiss();
  }

  async openWOrkClassSelection(){
    const _data = await this.modals.present(WorkClassesComponent, {plist: this.plist});
    let data = _data.data;
    if(data.data != 'A'){
      this.selectedWork[0] = data.item;
      this.entity.orderTypeId = this.selectedWork[0]['id']
    }
  }

  async openWOrkCommentSelection(){
    this.commentSubmitted = false;
    // const _data = await this.modals.present(WorkCommentComponent, {entity: this.entity}, 'small-modal');
    // let data = _data.data;
    // if(data.data != 'A'){
    //   console.log(data);
      let e = this.entity;
    //   console.log(e);
      let obj = {
        id: this.orderLines[0].id,
        double1: e.hoursSpent ? parseFloat(e.hoursSpent).toFixed(1) : 0.00,
        text1: e.description,
        text2: e.subOrderReferences
      }
      this.commentSpinner = true;
      console.log(obj);
      let flag = await this.works.setTaskComment(obj);
      this.commentSpinner = false;

      this.translation.getTranslateKey('info_data_update_success').then( v => {
        this.utility.presentSuccessToast(v)
      })
      

      // this.commentSubmitted = true;
      // setTimeout(() => {
      //   this.commentSubmitted = false;
      // }, 1500)
      
      // this.utility.presentSuccessToast('')

      // this.selectedWork[0] = data.item;
      // this.entity.orderTypeId = this.selectedWork[0]['id']
    // }
  }

}
