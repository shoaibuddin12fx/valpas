import { Component, Injector, Input, OnInit } from '@angular/core';
import { BasePage } from 'src/app/pages/base-page/base-page';

@Component({
  selector: 'app-work-classes',
  templateUrl: './work-classes.component.html',
  styleUrls: ['./work-classes.component.scss'],
})
export class WorkClassesComponent extends BasePage implements OnInit {

  id;
  @Input('plist') plist = [];

  constructor(injector: Injector) { 
    super(injector);
  }

  ngOnInit() {
    
  }
  
  selectSelector(item){
    this.modals.dismiss({item: item });
  }


  back(){
    this.modals.dismiss({data: 'A'});
  }

  



}
