import { Component, Injector, Input, OnInit } from '@angular/core';
import { BasePage } from 'src/app/pages/base-page/base-page';

@Component({
  selector: 'app-work-comment',
  templateUrl: './work-comment.component.html',
  styleUrls: ['./work-comment.component.scss'],
})
export class WorkCommentComponent extends BasePage implements OnInit {

  @Input('entity') entity: any;

  constructor(injector: Injector) { 
    super(injector);
  }

  ngOnInit() {}

  save(){ 
    this.modals.dismiss({entity: this.entity});
  }

  back(){
    this.modals.dismiss({data: 'A'});
  }


}
