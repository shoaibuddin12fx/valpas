import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MyWorkPageRoutingModule } from './my-work-routing.module';

import { MyWorkPage } from './my-work.page';
import { TranslateModule } from '@ngx-translate/core';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MyWorkPageRoutingModule,
    TranslateModule,
    Ng2SearchPipeModule
  ],
  declarations: [MyWorkPage]
})
export class MyWorkPageModule {}
