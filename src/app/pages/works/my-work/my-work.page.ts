import { Component, Injector, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { BobjectsService } from 'src/app/services/bobjects.service';
import { WorkMenuComponent } from '../works-detail/work-menu/work-menu.component';
import { WorksPage } from '../works.page';

@Component({
  selector: 'app-my-work',
  templateUrl: './my-work.page.html',
  styleUrls: ['./my-work.page.scss'],
})
export class MyWorkPage extends WorksPage implements OnInit, OnDestroy {

  plist: any[] = [];
  filters: any = {
    "fromDate":1577864643000,
    "toDate":1600078767000,
    "unallocated": false
  };
  constructor(injector: Injector, bobject: BobjectsService) { 
    super(injector, bobject);
   
    this.events.subscribe("reload:worklist", this.initialize.bind(this) )
  }

  ngOnDestroy(): void {
    // this.subscription.unsubscribe();
    // this.subscription = null;
  }

  ngOnInit() {
    console.log('ionViewDidEnter');
    localStorage.setItem('backBtnSub', 'true');

    let url = location.href;
    let splits = url.split('works/')
    console.log("is it", splits[1][0]);
    this.type = splits[1][0];    
    console.log(this.type);    




    this.initialize(null);
    }

  async initialize($event){

    // update date 
    var startDate = new Date();
    startDate.setMonth(startDate.getMonth() - 2);
    let sd = Math.round(startDate.getTime())
    
    var endDate = new Date();
    endDate.setMonth(endDate.getMonth() + 2);
    let ed = Math.round(endDate.getTime())

    this.filters['fromDate'] = sd;
    this.filters['toDate'] = ed;

    if(this.type == 'c'){

      console.log('propertyInOut IN')
      this.setPropertyInOut("IN");
      console.log(this.customer);

      let url = location.href;
      let splits = url.split('works/')
      let customer_id = splits[1].split('/')[0].replace('c', '');
      this.filters['propertyIds'] = [customer_id] 

    }

    const data = await this.getMyWorkList(this.filters);
    console.log(data);
    this.plist = data;

    if($event){
      $event.target.complete();
    }
  }

  async moreMenu($event){
    const _data = await this.popover.present(WorkMenuComponent, $event, {flag: 'ONW', pid: 0 });
    const data = _data.data;
    let flag = data.param;

    if(flag == 'R'){
      this.initialize(null);
    }
    
  }

  async completeTasks($event){

    let customer = JSON.parse(localStorage.getItem("customer"))
    let plistIds = this.plist.map( x => x.id);
    let data = {
      "ownerId": customer.buildingId, 
      "ids":plistIds
    }


    await this.works.postQuickCompleteTask(data)
    this.nav.pop()
  }

}
