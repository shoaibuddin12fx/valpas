import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OtherWorkPage } from './other-work.page';

const routes: Routes = [
  {
    path: '',
    component: OtherWorkPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OtherWorkPageRoutingModule {}
