import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OtherWorkPageRoutingModule } from './other-work-routing.module';

import { OtherWorkPage } from './other-work.page';
import { TranslateModule } from '@ngx-translate/core';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { FiltersComponent } from './filters/filters.component';
import { WorkMetersComponent } from '../work-meters/work-meters.component';
import { WorkMeterDetailComponent } from '../work-meters/work-meter-detail/work-meter-detail.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    OtherWorkPageRoutingModule,
    TranslateModule,
    Ng2SearchPipeModule
  ],
  declarations: [OtherWorkPage, FiltersComponent, WorkMetersComponent, WorkMeterDetailComponent]
})
export class OtherWorkPageModule {}
