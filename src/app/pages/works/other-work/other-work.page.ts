import { Component, Injector, OnInit } from '@angular/core';
import { BobjectsService } from 'src/app/services/bobjects.service';
import { WorkMetersComponent } from '../work-meters/work-meters.component';
import { WorkServiceComponent } from '../work-service/work-service.component';
import { WorkMenuComponent } from '../works-detail/work-menu/work-menu.component';
import { WorksPage } from '../works.page';
import { FiltersComponent } from './filters/filters.component';

@Component({
  selector: 'app-other-work',
  templateUrl: './other-work.page.html',
  styleUrls: ['./other-work.page.scss'],
})
export class OtherWorkPage extends WorksPage implements OnInit {

  plist: any[] = [];
  filters: any = {
    "fromDate":1577864643000,
    "toDate":1600078767000,
    "unallocated": false,
    "unallocatedOnly": true,
    "statusCode": null,
    "orderTypeIds": [],
    "costGroups": []
  };

  costs = [];
  statuses = [];
  types = [];


  constructor(injector: Injector, bobject: BobjectsService) { 
    super(injector, bobject);
    this.events.subscribe("reload:worklist", this.initialize.bind(this) )
  }

  ngOnInit() {
    
  }

  ionViewWillEnter(){
    let url = location.href;
    let splits = url.split('works/')
    console.log("is it", splits[1][0]);
    this.type = splits[1][0];    
    console.log(this.type);   


    this.initialize();
  }

  

  async initialize(){

    // update date 
    var startDate = new Date();
    startDate.setMonth(startDate.getMonth() - 10);
    let sd = Math.round(startDate.getTime())
    
    var endDate = new Date();
    endDate.setMonth(endDate.getMonth() + 2);
    let ed = Math.round(endDate.getTime())

    this.filters['fromDate'] = sd;
    this.filters['toDate'] = ed;

    if(this.type == 'c'){
      let url = location.href;
      let splits = url.split('works/')
      let customer_id = splits[1].split('/')[0].replace('c', '');
      this.filters['propertyIds'] = [customer_id]

      // month and year as int 
      // "year" and "month" p
      let date = new Date();
      this.filters['month'] = date.getMonth();
      this.filters['year'] = date.getFullYear();



    }

    this.plist = await this.getWorkList(this.filters);
    this.getFilters();
  }

  async moreMenu($event){
    console.log("open meun here", this.customer);
    const _data = await this.popover.present(WorkMenuComponent, $event, {flag: this.type == 'w' ? 'UNW' : 'UNWC', pid: 0 });
    const data = _data.data;
    console.log(data);

    let flag = data.param;

    if(flag == 'S'){
      this.openWorkFilters();
    }

    if(flag == 'M'){
      this.openMeterList();
    }
    
  }

  async openMeterList(){

    let customer = await this.sqlite.getActiveUser()
    console.log(customer)
    let _data = await this.modals.present(WorkMetersComponent, {item: customer});
    let data = _data.data;
    
  }

  async openWorkFilters(){
    let _data = await this.modals.present(FiltersComponent, {filters: this.filters, costs: this.costs, statuses : this.statuses, types: this.types  }, 'small-modal');
    let data = _data.data;

    if(data){
      console.log(data);
      if(data.data != 'A'){
        this.filters = data;
        this.plist = await this.getWorkList(this.filters);        
      }
    }
  }

  async getFilters(){

    this.costs = await this.works.getCostGroup() as [];
    this.statuses = await this.works.getWorkStatus() as [];
    this.types = await this.works.getWorkTypes() as [];

    const st = this.statuses.find( x => x.default == true );
    if(st){
      this.filters.statusCode = st.id;
    }

  }

  completeServiceTask($event, item){

    $event.stopPropagation();
    console.log(item);

  }

  async buttonAction($event, item){    

    if(this.type != 'c'){
      this.selectWork(item)
    }else{

      const _data = await this.popover.present(WorkMenuComponent, $event, {flag: 'CSW'} );
      let data = _data.data;

      if(data){
        let param = data.param

        item['buildingId'] = localStorage.getItem("customerBuildingId");
        switch(param){
          case "S":

            this.viewService(item);
          break  
        }



      }
      console.log(data);




      // const _data = this.modals.present(WorkMenuComponent, {flag: 'CSW'}, )
    }
    
    // this.type == 'c' ? item.showbuttons != item.showbuttons : this.selectWork(item)
    console.log(this.type, item);
  }

  viewService(item){
    this.modals.present(WorkServiceComponent, {item: item});
  }



}
