import { Component, Injector, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BasePage } from 'src/app/pages/base-page/base-page';

@Component({
  selector: 'app-work-meter-detail',
  templateUrl: './work-meter-detail.component.html',
  styleUrls: ['./work-meter-detail.component.scss'],
})
export class WorkMeterDetailComponent extends BasePage implements OnInit {

  // months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ]
  setMonth = new Date().toISOString();
  percentageReading = "";
  resultPercent = 0;
  isErrorInForm = false;
  mrvalue = 0;
  lmvalue = 0;
  plist = {
    
  }

  _item: boolean;
  aForm: FormGroup;
  get item(): boolean {
      return this._item;
  }
  @Input() set item(value: boolean) {
      this._item = value;
      this.initialize(value)
      
  }

  constructor(injector: Injector, public formBuilder: FormBuilder ) { 
    super(injector);
    this.setupForm();
  }

  setupForm() {

    var re = /\S+@\S+\.\S+/;
    
    this.aForm = this.formBuilder.group({
      prevMonthReading: [ 0 ],
      monthNumber: ['', Validators.compose([Validators.required])],
      curMonthReading: ['', Validators.compose([Validators.required])],
    })

  }

  ngOnInit() {}

  async initialize(value){
    console.log(value);

    // set month value 
    let date = new Date();
    if(date.getDay() >= 14){
      date.setMonth(date.getMonth() + 1)
      this.setMonth = date.toISOString() // this.months[date.getMonth() + 1]
    }else{
      date.setMonth(date.getMonth())
      this.setMonth = date.toISOString();
    }

    this.aForm.controls.prevMonthReading.setValue(value["prevMonthReading"]);
    this.aForm.controls.monthNumber.setValue(this.setMonth);


    let R = this.item['reference'] as number      
    let A = this.item['prevMonthReading'] as number

    if(!R || !A){
      this.isErrorInForm = true;
    }




  }

  async saveMeter(){

    if(this.resultPercent > 20){
      let okText = await this.translation.getTranslateKey("okText");
      let cancelText = await this.translation.getTranslateKey("cancelText");
      let do_you_want_to_progress = await this.translation.getTranslateKey("do_you_want_to_progress");
      let error_variation = await this.translation.getTranslateKey("error_variation") 
      const flag = await this.utility.presentConfirm(okText, cancelText, do_you_want_to_progress, error_variation  )

      if(!flag){
        return;
      }
    }



    if(this.aForm.invalid){
      return;
    }

    let formdata = this.aForm.value;
    formdata['monthNumber'] = new Date(formdata['monthNumber']).getMonth() + 1; 
    let t = this.item as any;
    const merge = {...t, ...formdata};

    console.log(merge);

    const data = await this.works.postSendMeterReading(merge)
    console.log(data);
    this.modals.dismiss()


  }

  async curMonthReadingChange($event){

    let val = $event.target.value;
    console.log(val);

    if(val){
      let R = this.item['reference'] as number
      let B = val as number;
      let A = this.item['prevMonthReading'] as number

      if(!R || R == 0){
        return;
      }

      this.resultPercent = Math.round(100 * ( B - A ) / R);
      if(this.resultPercent < 0){
        this.resultPercent = this.resultPercent * -1;
      }

      if(this.resultPercent > 20){
        this.percentageReading = await this.translation.getTranslateKey("error_variation") 
        this.percentageReading += ` (${this.resultPercent}%)`
      }else{
        this.percentageReading = "";
      }

      
      
    }

  }

}
