import { AfterViewInit, Component, ElementRef, Injector, Input, OnInit, ViewChild } from '@angular/core';
import { BasePage } from 'src/app/pages/base-page/base-page';
import { BobjectsService } from 'src/app/services/bobjects.service';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
@Component({
  selector: 'app-pictures-upload',
  templateUrl: './pictures-upload.component.html',
  styleUrls: ['./pictures-upload.component.scss'],
})
export class PicturesUploadComponent extends BasePage implements OnInit, AfterViewInit {
  
  @Input('item') item;
  @Input('camera') camera = false;
  @ViewChild('fileupload', { static: true }) fileupload: ElementRef<HTMLElement>;
  mimeType = '';
  image = '/assets/placeholder-1200.png';
  description = '';
  inputfile;
  imageUploadSectionIsHidden;
  constructor(injector: Injector, public bObject: BobjectsService, private cameraService:Camera) { 
    super(injector)


    

  }



  ngAfterViewInit(): void {
    console.log(this.camera);
    if(this.camera == true){
      console.log('camera opening')
      setTimeout(()=>{
        this.capturePicture()
      },500)
    }else{
      this.openFile();
    }
  }

  ngOnInit() {
  }

  openFile(){
    let el: HTMLElement = this.fileupload.nativeElement;
    el.click();
  }

  onSelectFile(event){

    console.log(event)
    let file = <File> event.target.files[0];
    if(file){

      this.inputfile = file;   
      this.mimeType = file.type;
      let reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = async (e: any) => {
        this.image = e.target.result;        
        console.log(this.image);
      }
      console.log(file);
    }        
  }

  async uploadFile(){

    console.log(this.item)

    if(!this.image){
      this.utility.presentFailureToast("Please upload a file")
    }
    
    var obj = await this.bObject.prepareObject();
    // check if file uploaded
    console.log(this.item);


    let formdata = new FormData();
    // formdata.append('file', this.inputfile, this.inputfile.name);
    formdata.append('description', this.description);
    formdata.append('id', this.item.orderId);
    formdata.append('type', 'ORDER');
    formdata.append('base64', this.image);

    let obj2 = {
      'description': this.description,
      'id': this.item.orderId,
      'type': 'ORDER',
      'base64': this.image
    }

    obj = {...obj, ...obj2}

    console.log(obj);
    for (const property in obj) {
      console.log(`${property}: ${obj[property]}`);
      formdata.append(property, obj[property]);
      // obj[]
    }
    // console.log(formdata);

    this.network.setWorkGallery(obj).then( v => {
      console.log(v);
      this.dismiss()
    }).catch( err => {
      console.error(err);
    })
  }
  

  capturePicture() {

    const options: CameraOptions = {
      quality: 100,
      destinationType: this.cameraService.DestinationType.DATA_URL,
      encodingType: this.cameraService.EncodingType.JPEG,
      mediaType: this.cameraService.MediaType.PICTURE,
      correctOrientation: true
    }

    this.cameraService.getPicture(options).then(async (imageData) => {
      this.image = 'data:image/jpeg;base64,' + imageData;
      this.mimeType = 'image/jpeg'
      
    })
      
  }

  unHideImageUploadSection(){
    this.imageUploadSectionIsHidden = !this.imageUploadSectionIsHidden;
  }

  dismiss(){
    this.modals.dismiss();
  }

}
