import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WorkGalleryComponent } from './work-gallery.component';

const routes: Routes = [
  {
    path: '',
    component: WorkGalleryComponent
  },
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WorkGalleryPageRoutingModule {}
