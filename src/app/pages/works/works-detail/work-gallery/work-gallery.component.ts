import { AfterViewInit, Component, ElementRef, Injector, Input, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { BasePage } from 'src/app/pages/base-page/base-page';
import { BobjectsService } from 'src/app/services/bobjects.service';
import { PicturesUploadComponent } from '../components/pictures-upload/pictures-upload.component';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
@Component({
  selector: 'app-work-gallery',
  templateUrl: './work-gallery.component.html',
  styleUrls: ['./work-gallery.component.scss'],
})
export class WorkGalleryComponent extends BasePage implements OnInit, AfterViewInit {

  @Input('item') item;
  @Input('camera') camera = false;
  @ViewChild('fileupload', { static: true }) fileupload: ElementRef<HTMLElement>;
  mimeType = '';
  plist = [];
  image;
  bObj;
  ids = [];
  checkbox_hidden: boolean;
  constructor(injector: Injector, public bObject: BobjectsService, router: ActivatedRoute, private cameraService:Camera,private photoViewer: PhotoViewer,private iab: InAppBrowser) { 
    super(injector)

    // this.initialize().then( () => {
    //   this.openModal();
    // });
    this.initialize()

    this.events.subscribe('Picture Clicked',(path)=>{
      this.openFileInModalView(path);
    })

  }
  ngAfterViewInit(): void { 
    if(this.camera == true){
      console.log('camera opening')
      setTimeout(()=>{
        this.capturePicture()
      },500)
    }else{
      // this.openFile();
    }
  }



  ngOnInit() {}

  ionViewWillEnter(){
    // this.initialize()
  }

  async initialize(){

    return new Promise( async resolve => {
      this.item = JSON.parse(this.nav.getQueryParams().item);
      this.camera = JSON.parse(this.nav.getQueryParams().camera);
  
      this.bObj = await this.bObject.prepareObject();
      console.log(this.bObj)
      console.log(this.item)
      this.bObj['id'] = this.item.orderId;
      this.bObj['type'] = "ORDER";
  
  
      
  
      this.network.getWorkGallery(this.bObj).then( v => {
        console.log(v);
        this.plist = v.entities
      })
  
      
      resolve(true);
    })


   




  }

  back(){
    this.nav.pop();
  }
  
  async openModal(){
    console.log('Camera here ', this.camera);
    const res = await this.modals.present(
      PicturesUploadComponent, 
      {item: this.item, camera: this.camera});
    this.initialize();


  }

  viewImage($event){
    this.openFileInModalView($event)
  }

  viewFile($event){
    if($event.includes('jpeg') || $event.includes('png')){
      this.openFileInModalView($event);
    }
    else{
      window.location.href = $event;
    }
  }

  openFile(){
    let el: HTMLElement = this.fileupload.nativeElement;
    el.click();
  }

  onSelectFile(event){

    console.log(event)
    let file = <File> event.target.files[0];
    if(file){

      this.mimeType = file.type;
      let reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = async (e: any) => {
        this.image = e.target.result;        
        console.log(this.image);
        this.uploadFile();
      }
      console.log(file);
    }        
  }

  async uploadFile(){

    console.log(this.item)

    if(!this.image){
     this.utility.presentFailureToast("Please upload a file")
     return
    }
    
    var obj = await this.bObject.prepareObject();
    // check if file uploaded
    console.log(this.item);

    let obj2 = {
      'description': "",
      'id': this.item.orderId,
      'type': 'ORDER',
      'base64': this.image
    }

    obj = {...obj, ...obj2}

    console.log(obj);    
    // console.log(formdata);

    this.network.setWorkGallery(obj).then( v => {
      console.log(v);      
      this.initialize()
      // this.events.publish('file:uploaded')
    }).catch( err => {
      console.error(err);
    })
  }
  

  capturePicture() {

    const options: CameraOptions = {
      quality: 100,
      destinationType: this.cameraService.DestinationType.DATA_URL,
      encodingType: this.cameraService.EncodingType.JPEG,
      mediaType: this.cameraService.MediaType.PICTURE,
      correctOrientation: true
    }

    this.cameraService.getPicture(options).then(async (imageData) => {
      this.image = 'data:image/jpeg;base64,' + imageData;
      this.mimeType = 'image/jpeg'
      this.uploadFile();
      
    })
      
  }

  async deleteFiles(){

    let ditems = this.plist.filter( x => x.checked == true);
    let flag = await this.utility.presentConfirm('Ok', 'Cancel', "Are you sure?", ditems.length + " will be deleted");

    if(flag){

      this.plist = this.plist.filter( x => {
        return !ditems.find( y => y.id == x.id );
      })
    }

    ditems.forEach(item =>{
      this.ids.push(item.id);
    })
    console.log(this.ids)
    var userImageDetails = {
      "username":this.bObj.username,
      "token":this.bObj.token,
      "ids": this.ids
    }
  this.network.removeImage(userImageDetails).then(data=>{
    console.log(data)
  })
    
  }

  hideCheckbox(){
    this.events.publish('checkbox_hidden');
  }

  openFileInModalView(path){
    this.photoViewer.show(path);
  }

  openPdf(path){
    this.iab.create(path,'blank');
  }

  
}
