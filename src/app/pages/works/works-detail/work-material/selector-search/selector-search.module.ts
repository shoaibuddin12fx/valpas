import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SelectorSearchPageRoutingModule } from './selector-search-routing.module';

import { SelectorSearchPage } from './selector-search.page';
import { TranslateModule } from '@ngx-translate/core';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SelectorSearchPageRoutingModule,
    TranslateModule,
    Ng2SearchPipeModule
  ],
  declarations: [SelectorSearchPage]
})
export class SelectorSearchPageModule {}
