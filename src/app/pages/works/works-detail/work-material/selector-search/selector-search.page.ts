import { Component, Inject, Injector, Input, OnInit } from '@angular/core';
import { BasePage } from 'src/app/pages/base-page/base-page';

@Component({
  selector: 'app-selector-search',
  templateUrl: './selector-search.page.html',
  styleUrls: ['./selector-search.page.scss'],
})
export class SelectorSearchPage extends BasePage implements OnInit {

  // @Input('id') id;

  _id: string;
  get id(): string {
    return this._id;
  }
  @Input() set id(value: string) {
    this._id = value;
    this.getStocks()
  }

  plist = [];
  new_selector;
  addSelector = true;
  storedData = {
    search: "",
    offset: 0
  }

  constructor(injector: Injector) { 
    super(injector);
    console.log(this.id);
    
  }

  ngOnInit() {
    // this.id = this.getParams().id;
    
  }
  
  selectSelector(item){
    this.events.publish("selectedSelector", item);
    this.modals.dismiss();
  }

  back(){
    this.modals.dismiss();
  }

  ionChange($event){
    let v = $event.target.value;
    console.log(v);
    this.storedData.offset = 0;
    this.storedData.search = v;
    this.getStocks();
  }

  getStockItemsAsIs(){
    return new Promise( async resolve => {
      let list = await this.works.getStorageSelectorsByNetwork();    
      console.log(list);
      await this.sqlite.setStocksInDatabase(list)
    })
  }

  getStocks(){

    return new Promise( async resolve => {

      // await this.getStockItemsAsIs();

      console.log(this.id, this.storedData.offset);
      if(this.storedData.offset == -1){
        resolve(true);
      }else{
        this.sqlite.getStockItems(this.id, this.storedData.search, this.storedData.offset).then((res: any) => {
          console.log(res);
          console.log("of", this.storedData.offset);
          if(this.storedData.offset == 0){
            this.plist = [];
            if(this.id && this.id != '-'){
              this.plist = res['stocks'].filter(value=>value.location==this.id);
            }
            else{
              this.plist = res['stocks'];
            }
            console.log(this.plist);
          }else{
            this.plist = this.plist.concat(res['stocks']);
          }

          this.storedData.offset = res['offset'];
          resolve(true);

        }, err => {})
      }


    })

  }

  loadMore($event){

    this.getStocks().then( v => {
      $event.target.complete();
    });

  }

  async addASelector(){
    console.log(this.new_selector)

    let item = {
      "id": null,
      "quantity": 0,
      "name": "",
      "display": ""
    }
    
    this.selectSelector(item)
    this.events.publish("selectedSelectorEditable");


  }

}
