import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WorkMaterialComponent } from './work-material.component';

const routes: Routes = [
  {
    path: '',
    component: WorkMaterialComponent
  },
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WorkMaterialPageRoutingModule {}
