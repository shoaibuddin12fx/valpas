import { AfterViewInit, Component, Injector, Input, OnInit, ViewChild } from '@angular/core';
import { AlertController, IonInput } from '@ionic/angular';
import { BasePage } from 'src/app/pages/base-page/base-page';
import { BobjectsService } from 'src/app/services/bobjects.service';
import { WorksDetailPage } from '../works-detail.page';
import { SelectorSearchPage } from './selector-search/selector-search.page';

@Component({
  selector: 'app-work-material',
  templateUrl: './work-material.component.html',
  styleUrls: ['./work-material.component.scss'],
})
export class WorkMaterialComponent extends BasePage implements OnInit, AfterViewInit {

  selectedStorage;
  storages;
  item;
  qty = 1;
  edit_selector = false;
  plist = [];
  new_selector;
  addSelector = true;
  defaultVal;
  storedData = {
    search: "",
    offset: 0
  }
  @ViewChild('description') description: IonInput;  
  selector;
  hideOptions = true;

  _id: boolean;
  get id(): boolean {
    return this._id;
  }
  @Input() set id(value: boolean) {
    this._id = value;
    // this.getStocks()
  }


  keyword = 'display';
  public data;


  constructor(injector: Injector, bobject: BobjectsService,public alertController: AlertController) { 
    super(injector);
    this.events.subscribe("selectedSelector", this.selectedSelector.bind(this));
    this.events.subscribe("selectedSelectorEditable", this.selectedSelectorEditable.bind(this) );
    this.initialize();
  }

  
  ionViewWillEnter(){
    
  }

  ngAfterViewInit(): void {
    
  }

  ngOnInit() {
    
  }

  



  initialize(){
    this.getStorage();
  }

  async getStorage(){
    this.storages = await this.works.getStorages();
    this.selectedStorage = this.storages.find( x => { return x.default == true });

    // this.selectedStorage = {
    //   "id": null,
    //   "quantity": 0,
    //   "name": "",
    //   "display": ""
    // }
    this.edit_selector = true;
    this.item = JSON.parse(localStorage.getItem('witem'));

    this.selectedSelector(this.item);
  }

  async openSelectors($event){

    const _data = await this.modals.present(SelectorSearchPage, {id: this.selectedStorage ? this.selectedStorage.name : null});
    // this.nav.push('works-detail/materials/selector-search/'+ this.selectedStorage.name);
  }

  changeSelectedStorage($event){
    let v = $event.target.value;
    this.selectedStorage = this.storages[v];
    console.log(this.selectedStorage);
  }

  async selectedSelector(item){
    console.log(item);
    this.selector = item;

    // get quantity
    this.selector.quantity = await this.works.getLocationQuantity(item)

  }

  selectedSelectorEditable(){
    
    this.edit_selector = true;
    setTimeout(() => {
      this.description.setFocus();
    }, 400);
  }

  back(){
    this.modals.modal.dismiss();
  }

  descriptionChange($event){
    let v = $event.target.value;
    console.log(v);
    this.selector.display = v;
    this.selector.name = v;
    
  }

  save(next = false){

    var self = this;
    return new Promise( async resolve => {

      if(this.edit_selector){       
        this.selector.orderId = localStorage.getItem('lineItemOrderId');
        this.selector.quantity = this.qty
        this.selector = await this.works.setNewOrderStorage(this.selector)
        .then( async ()=>{
          await this.presentAlert()
        })
        .catch(async (err) =>{
          await this.presentError()
        })
        this.events.publish("refresh:materiallist")  

      }else{
        console.log(this.item);
        if(this.item){
          console.log('saved')
          let obj = {
            "id": this.selector.id,
            "orderId": this.item.orderId,
            "quantity": this.qty
          }
          
          await this.works.setOrderStorage(obj)

          this.selector = null;  
          this.events.publish("refresh:materiallist")     
          resolve(true);

        }

      }

      

      

    })
    
  }

  async next(){

    await this.save()
    .then( async ()=>{
      await this.presentAlert()
    })
    .catch(async (err) =>{
      await this.presentError()
    })
    
    let index = this.storages.findIndex( x => {
      return x.id == this.selectedStorage.id
    });

    this.selectedStorage = this.storages[index + 1] ? this.storages[index + 1] : this.storages[0];
    this.qty = 0;
    this.selector = await this.works.getStorageSelectorById(this.selectedStorage.id)



  }
  // getStocks(search){

  //   return new Promise( async resolve => {

     
  //     this.storedData.offset = 0;
  //     console.log(this.id, this.storedData.offset);
  //     if(this.storedData.offset == -1){
  //       resolve(true);
  //     }else{
  //       this.sqlite.getStockItems(this.id, search, this.storedData.offset).then((res: any) => {

  //         console.log("of", this.storedData.offset);
  //         if(this.storedData.offset == 0){
            
  //           this.data = res['stocks'];
  //           this.selector = this.data;
  //           console.log(this.data);
  //         }else{
  //           this.data = this.data.concat(res['stocks']);
  //         }

  //         this.storedData.offset = res['offset'];
  //         resolve(true);

  //       }, err => {})
  //     }


  //   })

  // }
  addASelector(){

  }

  // selectEvent(item) {
  //   this.selector = item;
    
  // }

  // onChangeSearch(search: string) {
    
  //   this.getStocks(search)
  //   console.log(this.storedData.search);
    
  // }

  // onFocused(e) {
    
  // }


  async presentAlert() {
    const alert = await this.alertController.create({
      cssClass: 'custom',
      header: 'Success',
      message: 'Your Material has been saved',
      buttons: ['OK']
    });

    await alert.present();
  }

  async presentError() {
    const alert = await this.alertController.create({
      cssClass: 'custom',
      header: 'Failure',
      message: 'Something went wrong we could not save your material',
      buttons: ['OK']
    });

    await alert.present();
  }
}
