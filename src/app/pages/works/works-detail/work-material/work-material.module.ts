import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { WorkMaterialPageRoutingModule } from './work-material-routing.module';
import { WorkMaterialComponent } from './work-material.component';
import { SelectorSearchPageModule } from './selector-search/selector-search.module';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    WorkMaterialPageRoutingModule,
    TranslateModule,
    SelectorSearchPageModule,
    AutocompleteLibModule
  ],
  declarations: [WorkMaterialComponent],
  providers: [ DatePipe ] 
})
export class WorkMaterialPageModule {}
