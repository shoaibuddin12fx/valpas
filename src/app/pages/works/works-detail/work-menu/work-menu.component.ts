import { Component, Injector, Input, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { BobjectsService } from 'src/app/services/bobjects.service';
import { WorksPage } from '../../works.page';

@Component({
  selector: 'app-work-menu',
  templateUrl: './work-menu.component.html',
  styleUrls: ['./work-menu.component.scss'],
})
export class WorkMenuComponent implements OnInit {


  @Input() flag = 'C';
  @Input() customer;
  _pid: any;

  get pid(): any{
    return this._pid;
  }

  @Input() set pid(value: any) {
    this._pid = value;
  
  };

  constructor(public popoverCtrl: PopoverController) { 
    
  }
  ngAfterViewInit(): void {
    this.initialize();
  }

  ngOnInit() {}

  async initialize(){

    console.log(this.pid);
  }

  close(param) {
    this.popoverCtrl.dismiss({pid: this.pid, param: param });
  }

}
