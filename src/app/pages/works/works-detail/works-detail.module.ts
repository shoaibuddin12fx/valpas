import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { WorksDetailPageRoutingModule } from './works-detail-routing.module';
import { WorksDetailPage } from './works-detail.page';
import { TranslateModule } from '@ngx-translate/core';
import { WorkMenuComponent } from './work-menu/work-menu.component';
import { WorkGalleryComponent } from './work-gallery/work-gallery.component';
import { PicturesUploadComponent } from './components/pictures-upload/pictures-upload.component';
import {Camera} from '@ionic-native/camera';
import { WorkMaterialPageModule } from './work-material/work-material.module';
import { FileListComponent } from 'src/app/pages/works/works-detail/components/file-list/file-list.component';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    WorksDetailPageRoutingModule,
    TranslateModule,
    WorkMaterialPageModule,
    AutocompleteLibModule
  ],
  declarations: [WorksDetailPage, WorkMenuComponent, WorkGalleryComponent, PicturesUploadComponent,FileListComponent],
  providers: [ DatePipe ] 
})
export class WorksDetailPageModule {}
