import { AfterViewInit, Component, Injector, Input, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { BobjectsService } from 'src/app/services/bobjects.service';
import { BasePage } from '../../base-page/base-page';
import { CompleteWorkPage } from '../complete-work/complete-work.page';
import { WorkCompleteComponent } from './work-complete/work-complete.component';
import { WorkGalleryComponent } from './work-gallery/work-gallery.component';
import { WorkMenuComponent } from './work-menu/work-menu.component';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { WorkMaterialComponent } from './work-material/work-material.component';

@Component({
  selector: 'app-works-detail',
  templateUrl: './works-detail.page.html',
  styleUrls: ['./works-detail.page.scss'],
})
export class WorksDetailPage extends BasePage implements OnInit, AfterViewInit {

  user;
  isEquipmentStarted = false;
  @Input('asHistory') asHistory = false;

  object = [
    {
      orderpair: 1,
      label: ''
    }
  ]

  item: any;
  plist = [];
  // get item(): any {
  //   return this._item;
  // }

  // @Input() set item(value: any){
  //   this._item = value;
  //   console.log(value);

  // };



  constructor(injector: Injector, public bobject: BobjectsService, private cameraService:Camera) { 
    super(injector);

    this.sqlite.getActiveUser().then( user => {
      console.log(user);
      this.user = user 

    })
    this.events.subscribe("refresh:materiallist", this.getOrderLnines.bind(this))   

    
    
  }

  ngAfterViewInit(): void {
    this.item = JSON.parse(localStorage.getItem('witem'));
    this.getOrderLnines();
    console.log("break here", this.item);
  }

  ionViewWillEnter(){
    this.getOrderLnines()
  }

  async toggleStartTime($event){
    

    if(this.item.startedTime){
      // clock is ticking
      this.item.startedTime = null;

    }else{
      // clock is stopped
      var startDate = new Date();
      startDate.setMonth(startDate.getMonth() - 2);
      let sd = Math.round(startDate.getTime())

      this.item.startedTime = sd;

    }

    let data = {
      orderId: this.item.orderId,
      inOut: this.item.startedTime ? "IN" : "OUT"
    }

    const bobj = await this.bobject.prepareObject();
      var merged = {...bobj, ...data}
      console.log(merged)

    delete merged['timestamp'];  
    delete merged['id'];  


    this.network.getOrderStartTime(merged).then( v => {
      console.log(v);
      // this.events.publish("reload:worklist")
    })

  }

  ngOnInit() {
  }

  back(){

    if(this.asHistory){
      this.modals.dismiss();
    }else{
      this.nav.pop();
    }
    
  }

  async workMenu($event){
    console.log("open meun here");
    const _data = await this.popover.present(WorkMenuComponent, $event, {pid: this.item.orderId });
    const data = _data.data;
    console.log(data);
    if(data){
      let flag = data.param;
      if(flag == "M"){
        this.materialPage($event)
      }
  
      if(flag == "P"){
        this.galleryPage($event)
      }
      if(flag == "T"){
        this.galleryPage($event, true)
      }
    }



  }

  async completePage($event){
    localStorage.setItem('plist', JSON.stringify(this.plist));
    // this.nav.push('complete-work')
    this.modals.present(CompleteWorkPage)
  }

  async materialPage($event){
    const _data = await this.modals.present(WorkMaterialComponent) // .push(`works-detail/materials`)
  }

  async galleryPage($event, camera = false){
    console.log(camera)
    const _data = await this.nav.push(`works-detail/gallery`, {item: JSON.stringify(this.item), camera: camera })
    //const _data = await this.modals.present(WorkGalleryComponent, {item: this.item})
  }

  async getOrderLnines(){
    this.plist = await this.works.getOrderLinesByOrderId(this.item.orderId) as [];
    console.log(this.plist);
    if(this.plist.length > 0){
      console.log(this.plist[0].orderId);
      localStorage.setItem('lineItemOrderId',this.plist[0].orderId);
    }
  }

  async deleteLineItem(item){
    await this.works.removeOrderLinesByOrderId(item.id);
    this.plist = this.plist.filter( x => {
      return x.id != item.id
    })
  }

  callme(phone){
    this.utility.callMe(phone);
  }


}
