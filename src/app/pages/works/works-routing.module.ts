import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WorkMetersComponent } from './work-meters/work-meters.component';
import { WorkServiceComponent } from './work-service/work-service.component';

import { WorksPage } from './works.page';

const routes: Routes = [
  {
    path: '',
    component: WorksPage,
    children: [
      {
        path: '',
        redirectTo: 'my-work',
        pathMatch: 'full'
      },
      {
        path: 'my-work',
        loadChildren: () => import('./my-work/my-work.module').then( m => m.MyWorkPageModule)
      },
      {
        path: 'other-work',
        loadChildren: () => import('./other-work/other-work.module').then( m => m.OtherWorkPageModule)
      },
      {
        path: 'services-work',
        component: WorkServiceComponent
      },
      {
        path: 'work-meters',
        component: WorkMetersComponent
      },
    ]
  },
  // {
  //   path: 'complete-work',
  //   loadChildren: () => import('./complete-work/complete-work.module').then( m => m.CompleteWorkPageModule)
  // },
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WorksPageRoutingModule {}
