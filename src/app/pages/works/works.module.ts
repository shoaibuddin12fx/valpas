import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { WorksPageRoutingModule } from './works-routing.module';

import { WorksPage } from './works.page';
import { TranslateModule } from '@ngx-translate/core';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { WorksDetailPageModule } from './works-detail/works-detail.module';
import { WorkServiceComponent } from './work-service/work-service.component';
import { WorkMeterDetailComponent } from './work-meters/work-meter-detail/work-meter-detail.component';
import { WorkHistoryComponent } from './work-history/work-history.component';
import { WorksDetailPage } from './works-detail/works-detail.page';
import { Camera} from '@ionic-native/camera';
import { WorkMaterialComponent } from './works-detail/work-material/work-material.component';
import { FileListComponent } from 'src/app/pages/works/works-detail/components/file-list/file-list.component';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    WorksPageRoutingModule,
    TranslateModule,
    Ng2SearchPipeModule,
    WorksDetailPageModule,
    AutocompleteLibModule
  ],
  declarations: [WorksPage, WorkMaterialComponent, WorkServiceComponent, WorkHistoryComponent,FileListComponent]
})
export class WorksPageModule {}
