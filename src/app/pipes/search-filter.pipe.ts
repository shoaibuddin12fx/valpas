import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchFilter'
})
export class SearchFilterPipe implements PipeTransform {

  transform(items: any[], filter: any[]): any {
    if (!items || !filter) {
        return items;
    }
    // filter items array, items which match and return true will be
    // kept, false will be filtered out

    const keys = Object.keys(filter);
    console.log(keys);

    return items.filter(item => {
      
      var flag = false;

      for(var i = 0;i < keys.length; i++){
        console.log(keys);
        flag = item[keys[i]].toLowerCase().indexOf(filter[keys[i]].toLowerCase()) !== -1;
        if(flag){
          break;
        }  
      }

      return flag;
      
    });



  }

}
