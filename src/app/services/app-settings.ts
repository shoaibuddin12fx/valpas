export const AppSettings = {
    'IS_FIREBASE_ENABLED': false,
    'SHOW_START_WIZARD': false,
    'SUBSCRIBE': false,
    'FIREBASE_CONFIG': {
      'apiKey': "AIzaSyB4jV4_XwFbiQCpWJ8UVWhnjXoDZfup4I4",
      'authDomain': "ionic-necco-theme.firebaseapp.com",
      'databaseURL': "https://ionic-necco-theme.firebaseio.com",
      'projectId': "onic-necco-theme",
      'storageBucket': "onic-necco-theme.appspot.com",
      'messagingSenderId': "553452197105",
      'appId': "1:553452197105:web:49b7d8435def54c6578550",
      'measurementId': "G-M60K4G09FY"
    }
};
