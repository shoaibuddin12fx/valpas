import { Injectable } from '@angular/core';
import { BobjectsService } from './bobjects.service';
import { NetworkService } from './network.service';

@Injectable({
  providedIn: 'root'
})
export class CalendarService {

  eventSource = []; 
  public selectedEvent: any;
  constructor(public bobject: BobjectsService,public network: NetworkService) { }

  getfirstLastRange(start, end){    
    var firstDay = new Date(this.UTCDate(start)).getTime();
    var lastDay = new Date(this.UTCDate(end)).getTime();
    console.log(firstDay, lastDay)
    return {
      fromDate: firstDay,
      toDate: lastDay
    }
  }

  async initializeData(dates){

    return new Promise( async resolve => {
      const _data = await this.bobject.prepareObject();
      // const dates = {
      //   fromDate: obj['start'],// 1609459200000,
      //   toDate: obj['end'] // 1612051200000
      // }
      const merged = {..._data, ...dates}

      let own = await this.network.getAllMyWorks(merged);
      // let own = await this.network.getAllResourceView(_data);
      console.log(own)
      // console.log(own.entities);
      // let order = own.entities.find( x => {
      //   return x.orders.length > 1;
      // });
      // var events = []
      if(own.orders.length > 0){
        console.log(own.orders)
        await this.transformEvents(own.orders)
      }
  
      // console.log(this.eventSource);
      resolve(true);
    })

    
  }

  transformEvents(orders){
    return new Promise( resolve => {
      var events = [];
      for (var i = 0; i < orders.length; i++) {
        let item = orders[i];
        // var startMinute = Math.floor(Math.random() * 24 * 60);
        // var endMinute = Math.floor(Math.random() * 180) + startMinute;
        let startTime = new Date(item['deliveryTime']);
        let endTime = new Date(item['deliveryTime']);

        let obj = {
          title: item['building'],
          startTime: this.UTCDate(item['deliveryTime']),
          endTime: this.UTCDate(item['deliveryEndTime']),
          allDay: false,
          data: orders[i]
        }

        console.log(obj)
        
        events.push(obj);
      }
      this.eventSource = events;
      resolve(true)
    })
    
  }

  UTCDate(unix){
    // var unix = 1610521200000;
    var date = new Date(unix)
    date.setMinutes(date.getMinutes() + date.getTimezoneOffset())

    let y = date.getFullYear();
    let m = date.getMonth();
    let d = date.getDate();
    let h = date.getHours();
    let _m = date.getMinutes();
    let s = date.getMinutes();
    console.log(y,m,d,h,_m,s)
    return new Date(Date.UTC(y,m,d,h,_m,s));
  }
}
