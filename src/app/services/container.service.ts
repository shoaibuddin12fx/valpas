import { IService } from './IService';
import { AngularFireDatabase } from '@angular/fire/database';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppSettings } from './app-settings';
import { LoadingService } from './loading-service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ContainerService implements IService {

  constructor(public af: AngularFireDatabase, 
    private loadingService: LoadingService,
    private http:HttpClient) { }


  getAllThemes(): any[] {
    throw new Error("Method not implemented.");
  }
  getTitle(): string {
    throw new Error("Method not implemented.");
  }
  load(menuItem: any) {
    throw new Error("Method not implemented.");
  }

  getContainerList(): Observable<any> {
    var url = environment.SERVER_URL+"/rest/containers/list";
   
    var headers = new HttpHeaders({
      'Access-Control-Allow-Origin': '*',
      'Content-Type': 'application/json'
    });

    let request = {
      'username': localStorage.getItem(environment.USER_NAME),
      'token': localStorage.getItem(environment.TOKEN),
      'locale': 'en_US',
      'projectId':localStorage.getItem(environment.PROJECT_CODE)
      }
    return this.http.post(url,request, {headers: headers});
  }
}
