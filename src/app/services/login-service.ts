import { IService } from './IService';
import { AngularFireDatabase } from '@angular/fire/database';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppSettings } from './app-settings';
import { environment } from '../../environments/environment';
import { EncryptService } from '../encrypt.service';
import { NetworkService } from './network.service';
import { BobjectsService } from './bobjects.service';

@Injectable({ providedIn: 'root' })
export class LoginService implements IService {

  constructor(public af: AngularFireDatabase, 
    public network: NetworkService,
    public encrypt: EncryptService) { }

  getTitle = (): string => 'Login pages';

  getAllThemes = (): Array<any> => {
    return [
      { 'url': 'login/0', 'title': 'Login + logo 1', 'theme': 'layout1' },
      { 'url': 'login/1', 'title': 'Login + logo 2', 'theme': 'layout2' }
    ];
  }

  getDataForTheme = (menuItem: any): Array<any> => {
    return this[
      'getDataFor' +
      menuItem.theme.charAt(0).toUpperCase() +
      menuItem.theme.slice(1)
    ]();
  }

  //* Data Set for page 1
  getDataForLayout1 = (): any => {
    return {
      'headerTitle': 'Login + logo 1',
      "username": "Enter your username",
      "password": "Enter your password",
      "labelUsername": "USERNAME",
      "labelPassword": "PASSWORD",
      "register": "Register now!",
      "forgotPassword": "Forgot password?",
      "login": "Login",
      "loginFacebook": "Login With Facebook",
      "subtitle": "Login",
      "title": "Login to your account",
      "description": "Don't have account?",
      "skip": "Skip",
      "logo": "assets/imgs/logo/logo.png",
    };
  }

  //* Data Set for page 2
  getDataForLayout2 = (): any => {
    return {
      'headerTitle': 'Login + logo 2',
      "forgotPassword": "Forgot password?",
      "subtitle": "Welcome",
      "labelUsername": "USERNAME",
      "labelPassword": "PASSWORD",
      "title": "Login to your account",
      "username": "Enter your username",
      "password": "Enter your password",
      "register": "Register",
      "login": "Login",
      "skip": "Skip",
      "facebook": "Facebook",
      "twitter": "Twitter",
      "google": "Google",
      "pinterest": "Pinterest",
      "description": "Don't have account?",
      "logo": "assets/imgs/logo/2.png",
    };
  }

  load(item: any): Observable<any> {
    //this.loadingService.show();
    if (AppSettings.IS_FIREBASE_ENABLED) {
      return new Observable(observer => {
        this.af
          .object('login/' + item.theme)
          .valueChanges()
          .subscribe(snapshot => {
      //      this.loadingService.hide();
            observer.next(snapshot);
            observer.complete();
          }, err => {
        //    this.loadingService.hide();
            observer.error([]);
            observer.complete();
          });
      });
    } else {
      return new Observable(observer => {
        //this.loadingService.hide();
        observer.next(this.getDataForTheme(item));
        observer.complete();
      });
    }
  }

  login(param: any): Promise<any> {
  
    return new Promise( (resolve, reject) => {

      console.log(param);
      var dataObj = { username: this.encrypt.set(param.username), password: this.encrypt.set(param.password), isMobile: true };

      


      this.network.login(dataObj).then( result => {
        console.log(result);
        resolve(result);
      }, err => {
        console.log(err);
        reject()
      })

    })
    // var url = environment.SERVER_URL+"/rest/login";

    // var dataObj = { username: this.encrypt.set(param.username), password: this.encrypt.set(param.password), isMobile: true };
    
    // console.log(dataObj);

    // return this.http.post(url, dataObj, { observe: 'response' });

      /* return new Observable(observer => {
        this.af
          .object('login/' + item.theme)
          .valueChanges()
          .subscribe(snapshot => {
            this.loadingService.hide();
            observer.next(snapshot);
            observer.complete();
          }, err => {
            this.loadingService.hide();
            observer.error([]);
            observer.complete();
          });
      }); */
    
  }
}
