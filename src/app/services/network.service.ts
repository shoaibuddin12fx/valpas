import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { ApiService } from './api.service';
import { EventsService } from './events.service';
import { UtilityService } from './utility.service';
import { File } from '@ionic-native/file/ngx';

import { Plugins, FilesystemDirectory, FilesystemEncoding } from '@capacitor/core';
import { SqliteService } from './sqlite.service';
import { HttpHeaders } from '@angular/common/http';
const { Filesystem } = Plugins;

@Injectable({
  providedIn: 'root'
})
export class NetworkService {


  login(data) {
    return this.httpPostResponse('login', data, null, true)
  }

  getUser(data, loader = false, error = false) {
    return this.httpPostResponse('users', data, null, loader, error)
  }

  getItsQrcode(data){
    return this.httpPostResponse('tasks/qrcode', data, null, true)
  }

  getItsQrcodeInOut(data){
    return this.httpPostResponse('tasks/inOut', data, null, true)
  }

  getAllProjects(data){
    return this.httpPostResponse('projects/list', data, null, true)
  }

  getAllLitteras(data){
    return this.httpPostResponse('litteras', data, null, true)
  }

  getAllWorks(data){
    return this.httpPostResponse('orders/list', data, null, false)
  }

  getServicebookList(data){
    return this.httpPostResponse('servicebook/task', data, null, false)
  }

  getAllMyWorks(data){
    return this.httpPostResponse('orders/own', data, null, true)
  }

  getWorkGallery(data){
    return this.httpPostResponse('file/list', data, null, true, true) // 'multipart/form-data'
  }

  setWorkGallery(data){
    return this.httpPostResponse('file/upload/base', data, null, true, true) // 'multipart/form-data'
  }

  setPropertyInOut(data){
    return this.httpPostResponse('propertyInOut', data, null, false)
  }

  getAllCustomers(data){
    return this.httpPostResponse('buildings', data, null, true)
  }

  getServiceListByCustomerId(data){ 
    return this.httpPostResponse('getServiceTasks', data, null, true)
  }

  getAllServicemen(data){
    return this.httpPostResponse('users/servicemen', data, null, true)
  }

  getAllResourceView(data){
    return this.httpPostResponse('resources/getResourceViewData', data, null, true)
  }

  getOrderStartTime(data){
    return this.httpPostResponse('srInOut', data, null, true)
  }

  getStorages(data){
    return this.httpPostResponse('stockitems/locations/list', data, null, false)
  }

  getStoragesById(data){
    return this.httpPostResponse('stockitems/selectors', data, null, false)
  }

  postCompleteOrder(data){
    return this.httpPostResponse('completeSR', data, null, false)
  }

  postQuickCompleteTask(data){
    return this.httpPostResponse('task/complete/quick', data, null, true)
  }

  setTaskComment(data){
    return this.httpPostResponse('task/comment', data, null, false)
  }
  
  getAddMaterielLine(data){
    return this.httpPostResponse('addMaterielLine', data, null, false)
  }

  getOrderLines(data){
    return this.httpPostResponse('orders/lines', data, null, false)
  }

  getOrderTypes(data){
    return this.httpPostResponse('orders/types', data, null, false)
  }

  removeOrderLines(data){
    return this.httpPostResponse('button/orderline/delete', data, null, false)
  }

  getCostGroup(data){
    return this.httpPostResponse('resources/costGroups', data, null, false)
  }

  getWorkTypes(data){
    return this.httpPostResponse('orders/types', data, null, false)
  }

  getWorkStatus(data){
    return this.httpPostResponse('orders/statuses', data, null, false)
  }

  getLocationQuantity(data){
    return this.httpPostResponse('stockitems/locations/quantity', data, null, false)
  }

  getMetersData(data){
    return this.httpPostResponse('meters', data, null, false)
  }

  postCompleteServiceTask(data){
    return this.httpPostResponse('completeServiceTasks', data, null, false)
  }

  postSendMeterReading(data){
    return this.httpPostResponse('meterReading/edit', data, null, false)
  }

  getPropertyItemById(data){
    return this.httpPostResponse('getPropertyInfo', data, null, false)
  }

  getTokenRefresh(data){
    return this.httpPostResponse('login/refresh', data, null, false)
  }

  getImageDetail(data){
    return this.httpPostResponse('file/path',data,null,false)
  }

  removeImage(data){
    return this.httpPostResponse('file/remove',data,null,false)
  }




  constructor(
    public utility: UtilityService,
    public api: ApiService,
    private events: EventsService,
    private platform: Platform,
    private file: File,
    public sqlite: SqliteService
  ) {
    // console.log('Hello NetworkProvider Provider');
  }

  httpPostResponse(key, data, id = null, showloader = false, showError = true, contenttype = 'application/json') {
    return this.httpResponse('post', key, data, id, showloader, showError, contenttype);
  }

  httpGetResponse(key, id = null, showloader = false, showError = true, contenttype = 'application/json') {
    return this.httpResponse('get', key, {}, id, showloader, showError, contenttype);
  }

  // default 'Content-Type': 'application/json',
  httpResponse(type = 'get', key, data, id = null, showloader = false, showError = true, contenttype = 'application/json'): Promise<any> {

    return new Promise( ( resolve, reject ) => {

      // console.log(key);
      let sent = data ? data : {};
      if (showloader == true) {
        this.utility.showLoader();
      }

      const _id = (id) ? '/' + id : '';
      const url = key + _id;

      let reqOpts = {
        headers: new HttpHeaders({
          'Content-Type':  contenttype,
        })
      }

      // console.info(url);
      const seq = (type == 'get') ? this.api.get(url, {}, reqOpts) : this.api.post(url, data, reqOpts);

      seq.subscribe((data: any) => {

        // console.log(data);
        let res = data['responseData'];
        this.addLogsInDevice({
          type: type,
          url: url,
          sent: JSON.stringify(sent),
          data: JSON.stringify(data),
          timestamp: Date.now() / 1000
        })

        if (showloader == true) {
          this.utility.hideLoader();
        }

        // console.log(data['code'])
        if (data['code'] != 200) {
          if (showError) {
            const myVar = data['responseData'];
            if (typeof myVar === 'string' || myVar instanceof String){
              this.utility.presentSuccessToast(data['responseData']);
            }else{
              if (!data['responseData']){
                this.utility.presentSuccessToast("Request Failed");
                reject(null);
                return;
              }
              if(!data['responseData']['feedback']){
                this.utility.presentSuccessToast("Request Failed");
                reject(null);
                return;
              }

              let failures = data['responseData']['feedback']['failures'];
              if(failures.length > 0){
                let msg = failures[0]['name'];
                if(msg){
                  this.utility.presentSuccessToast(msg);
                }else{
                  this.utility.presentSuccessToast("Request Failed");
                }
                
              }else{
                this.utility.presentSuccessToast("Request Failed");
              }

              
            }

            
          }
          reject(null);
        } 
        
        if(data['code'] == 401){
          this.events.publish("user:logout");
        }
        else {
          resolve(res);
        }

      }, err => {

        this.addLogsInDevice({
          type: type,
          url: url,
          sent: JSON.stringify(sent),
          data: JSON.stringify(err),
          timestamp: Date.now() / 1000
        })

        let error = err;
        if (showloader == true) {
          this.utility.hideLoader();
        }

        if (showError) {
          if(data['code'] != 401){
            this.utility.presentFailureToast(error['responseData']);
          }
        }

        console.log(err);

        reject(null);

      });

    });

  }

  addLogsInDevice(data){
    this.platform.ready().then(() => {

      this.sqlite.setLogInDatabase(data)
    // //  this.appendToFile(data)
    //   this.file.checkDir(this.file.dataDirectory, 'valpas-logs')
    //   .then(_ => {
    //     // if directory exist ... insert logs
    //     this.appendToFile(data)

    //   })
    //   .catch(async err => {
    //     // if directory doesn't exist .. create one and insert logs
    //     await this.file.createDir(this.file.dataDirectory , 'valpas-logs', false);
    //     this.appendToFile(data)
        
    //   })

    });

  }

  async appendToFile(data){
    // let blob = new Blob(data, { type: 'application/json' });
    
    // this.file.writeFile(this.file.dataDirectory+'/valpas-logs', 'valpas-logs.json', blob, {replace: false, append: true});
    // this.file.writeExistingFile(this.file.dataDirectory+'/valpas-logs', 'valpas-logs.txt', blob);
  }

}
