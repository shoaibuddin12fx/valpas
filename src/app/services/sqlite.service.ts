import { Injectable } from '@angular/core';
import { SQLite, SQLiteDatabaseConfig, SQLiteObject } from '@ionic-native/sqlite/ngx';
import { Platform } from '@ionic/angular';
import { NetworkService } from './network.service';
import { UtilityService } from './utility.service';
import { StorageService } from './storage.service';
import { browserDBInstance } from './browser-db-instance'
import { error } from 'protractor';

const menu_json = require('../data/sidemenu.json');

declare var window: any;
const SQL_DB_NAME = '__valpas.db';

@Injectable({
  providedIn: 'root'
})
export class SqliteService {

  db: any;
  config: SQLiteDatabaseConfig = {
    name: 'zuul_systems.db',
    location: 'default'
  }
  
  public msg = "Sync In Progress ...";

  constructor(
    private storage: StorageService, 
    private platform: Platform, 
    private sqlite: SQLite, 
    ) {

      


  }

  public initialize() {

    return new Promise(resolve => {
      this.storage.getKey('is_database_initialized').then(async v => {
        if (!v) {
          await this.initializeDatabase();
          resolve(true);
        } else {
          resolve(true);
        }
      })
    })

  }

  async initializeDatabase() {

    return new Promise(async resolve => {
      await this.platform.ready();
      // initialize database object
      await this.createDatabase()
      
      // initialize all tables

      // initialize users table
      await this.initializeUsersTable();
      // initialize the user flags table for screens
      await this.initializeSidemenuTable();
      // initialize stock table
      await this.initializeStockTable();
      // initialize customers table
      await this.initializeCustomerTable();
      
      await this.initializeLogTable();

      await this.initializeUrlTable();
      
      this.storage.setKey('is_database_initialized', true);
      resolve(true);
    })


  }

  async createDatabase(){
    return new Promise( async resolve => {
      if(this.platform.is('cordova')){
        await this.sqlite.create(this.config).then(db => {
          this.msg = 'Database initialized';
          this.db = db
        });
      }else{
        let _db = window.openDatabase(SQL_DB_NAME, '1.0', 'DEV', 5 * 1024 * 1024);
        this.db = browserDBInstance(_db);
        this.msg = 'Database initialized';
        
      }
      resolve();
    })
    

  }

  async initializeUsersTable() {

    return new Promise(resolve => {
      // create statement
      var sql = "CREATE TABLE IF NOT EXISTS users(";
      sql += "id INTEGER PRIMARY KEY, ";
      sql += "accountId INTEGER DEFAULT 0, " 
      sql += "creationDate TEXT, " 
      sql += "email TEXT, " 
      sql += "firstname TEXT, " 
      sql += "industry INTEGER DEFAULT 0, " 
      sql += "lastname TEXT, " 
      sql += "phoneNumber TEXT, " 
      sql += "project TEXT, " 
      sql += "refreshToken TEXT, " 
      sql += "role_id INTEGER, " 
      sql += "role_name TEXT, " 
      sql += "startApi TEXT, " 
      sql += "success TEXT, " 
      sql += "token TEXT, " 
      sql += "userId INTEGER, " 
      sql += "username TEXT, "
      sql += "active INTEGER DEFAULT 0 "; 
      sql += ")";

      this.msg = 'Initializing Users ...';
      resolve(this.execute(sql, []));
    })

  }

  async initializeSidemenuTable() {

    return new Promise(resolve => {
      // create statement
      var sql = "CREATE TABLE IF NOT EXISTS sidemenu(";
      sql += "user_id INTEGER, ";
      sql += "title TEXT, ";
      sql += "route TEXT, ";
      sql += "url TEXT, ";
      sql += "theme TEXT, ";
      sql += "icon TEXT, ";
      sql += "listView BOOLEAN DEFAULT false, ";
      sql += "component TEXT, ";
      sql += "singlePage BOOLEAN DEFAULT false, ";
      sql += "toggle BOOLEAN DEFAULT false, ";
      sql += "orderpair INTEGER, ";
      sql += "UNIQUE(user_id, route)"

      sql += ")";

      this.msg = 'Initializing User Menu ...';
      resolve(this.execute(sql, []));
    })

  }

  async initializeStockTable() {

    return new Promise(resolve => {
      // create statement
      var sql = "CREATE TABLE IF NOT EXISTS stocks(";
      sql += "id INTEGER PRIMARY KEY, ";
      sql += "display TEXT, " 
      sql += "location TEXT, " 
      sql += "name TEXT, " 
      sql += "orderId INTEGER, " 
      sql += "productCode TEXT, " 
      sql += "quantity INTEGER DEFAULT 1, " 
      sql += "salesPrice INTEGER DEFAULT 0, " 
      sql += "unit TEXT "             
      sql += ")";

      this.msg = 'Initializing Stocks ...';
      resolve(this.execute(sql, []));
    })

  }

  async initializeCustomerTable() {

    return new Promise(resolve => {
      // create statement
      var sql = "CREATE TABLE IF NOT EXISTS buildings(";
      sql += "id INTEGER PRIMARY KEY, ";
      sql += "buildingId INTEGER, ";
      sql += "city TEXT, " 
      sql += "name TEXT, " 
      sql += "streetAddress TEXT, " 
      sql += "propertyId INTEGER "   
      sql += ")";

      this.msg = 'Initializing buildings ...';
      resolve(this.execute(sql, []));
    })

  }

  async initializeFlagsTable() {

    return new Promise(resolve => {
      // create statement
      var sql = "CREATE TABLE IF NOT EXISTS user_flags(";
      sql += "id INTEGER PRIMARY KEY, ";
      sql += "dashboard BOOLEAN DEFAULT false, ";
      sql += "sync_contact_from_phone BOOLEAN DEFAULT false, ";
      sql += "sync_contact_from_zuul BOOLEAN DEFAULT false, ";
      sql += "google_map BOOLEAN DEFAULT false, ";
      sql += "active_pass_list BOOLEAN DEFAULT false, ";
      sql += "archive_pass_list_archive BOOLEAN DEFAULT false, ";
      sql += "archive_pass_list_sent BOOLEAN DEFAULT false, ";
      sql += "archive_pass_list_scanned BOOLEAN DEFAULT false, ";
      sql += "sent_pass_list BOOLEAN DEFAULT false, ";
      sql += "pass_details BOOLEAN DEFAULT false, ";
      sql += "notifications BOOLEAN DEFAULT false, ";
      sql += "request_a_pass BOOLEAN DEFAULT false, ";
      sql += "create_new_pass BOOLEAN DEFAULT false )";

      this.msg = 'Initializing User Flags ...';
      resolve(this.execute(sql, []));
    })

  }

  async initializeLogTable(){

    return new Promise(resolve => {
      // create statement
      var sql = "CREATE TABLE IF NOT EXISTS logs(";
      sql += "url text, ";
      sql += "type text, ";
      sql += "sent text, ";      
      sql += "data text, ";      
      sql += "timestamp )";

      this.msg = 'Initializing User Flags ...';
      resolve(this.execute(sql, []));
    })

  }

  async initializeUrlTable(){

    return new Promise(resolve => {
      // create statement
      var sql = "CREATE TABLE IF NOT EXISTS urls(";
      sql += "id INTEGER PRIMARY KEY, ";
      sql += "url text, ";
      sql += "active integer";    
      sql += " )";

      this.msg = 'Initializing Urls ...';
      resolve(this.execute(sql, []));
    })

  }

  // LOG QUERY START

  public async setUrlsInDatabase(list){
    
    return new Promise( async resolve => {

      // get sidemenu json data from file and dump into sqlite table        
      for (var i = 0; i < list.length; i++) {
        var sql = "INSERT OR REPLACE INTO logs(";

        sql += "id, ";
        sql += "url, ";
        sql += "active ";     

        sql += ") ";

        sql += "VALUES (";
        
        sql += "?, "
        sql += "?, "
        sql += "? "       
        sql += ")";

        var values = [
          list[i]["id"],
          list[i]["url"],
          0          
        ];

        await this.execute(sql, values);
      }


      resolve(true);


    })

}

public async getUrlsInDatabase(){

  return new Promise(async resolve => {
    let sql = "SELECT * FROM urls order by active desc";
    let values = [];

    let d = await this.execute(sql, values);
    // var data = d as any[];
    const data = this.getRows(d);
    if (data.length > 0) {
      resolve(data);
    } else {
      resolve([]);
    }

  })
}

public async setActiveUrlInDatabase(id){



}

// LOG QUERY ENDS

  // LOG QUERY START

  public async setLogInDatabase(log){
    
      return new Promise( async resolve => {
  
        // get sidemenu json data from file and dump into sqlite table        
  
        var sql = "INSERT OR REPLACE INTO logs(";

        sql += "url, ";
        sql += "type, ";
        sql += "sent, ";
        sql += "data, ";
        sql += "timestamp ";    

        sql += ") ";
  
        sql += "VALUES (";
  
        sql += "?, "
        sql += "?, " 
        sql += "?, " 
        sql += "?, " 
        sql += "? "         
        sql += ")";

        var values = [
          log["url"],
          log["type"],
          log["sent"],
          log["data"],
          log["timestamp"]         
        ];
  
        await this.execute(sql, values);
        resolve(true);
  
  
      })

  }

  public async getLogInDatabase(){

    return new Promise(async resolve => {
      let sql = "SELECT * FROM logs order by timestamp desc";
      let values = [];

      let d = await this.execute(sql, values);
      // var data = d as any[];
      const data = this.getRows(d);
      if (data.length > 0) {
        resolve(data);
      } else {
        resolve([]);
      }

    })
  }

  public async clearLogInDatabase(){

    return new Promise(async resolve => {
      let sql = "DELETE FROM logs";
      let values = [];

      let d = await this.execute(sql, values);
      // var data = d as any[];
      const data = this.getRows(d);
      if (data.length > 0) {
        resolve(data);
      } else {
        resolve([]);
      }

    })
  }

  // LOG QUERY ENDS

  // Building QUERY STARTS

  public async getBuildingCount(){

  }

  public async deleteAllBuildings(){

    return new Promise( async resolve => {

      let sql = "DELETE FROM buildings";
      let values = [];

      let d = await this.execute(sql, values);
      resolve(d);

    })

  }

  public async setBuildingsInDatabase(list){

    
    return new Promise( async resolve => {

      await this.deleteAllBuildings()

      let insertRows = [];

      // get sidemenu json data from file and dump into sqlite table
      for (var i = 0; i < list.length; i++) {

        var sql = "INSERT OR REPLACE INTO buildings(";
        sql += "id, ";
        sql += "buildingId, ";
        sql += "city, ";
        sql += "name, ";
        sql += "streetAddress, ";
        sql += "propertyId ";
        sql += ") ";
  
        sql += "VALUES (";
  
        sql += "?, "
        sql += "?, " 
        sql += "?, " 
        sql += "?, " 
        sql += "?, " 
        sql += "? " // 6               
        sql += ")";

        var values = [
          list[i]["id"],
          list[i]["buildingId"],
          list[i]["city"],
          list[i]["name"],
          list[i]["streetAddress"],
          list[i]["propertyId"]              
        ];
  
        // await this.execute(sql, values);
        insertRows.push([
          sql,
          values
        ]);

      }

      await this.prepareBatch(insertRows)

      resolve(true);


    })
  }

  removeBuildingsItemById() {

    return new Promise(async resolve => {
      let sql = "DELETE FROM buildings";
      let values = [];
      await this.execute(sql, values);
      resolve(true);
    })

  }

  public async getBuildingsItems(search = null, offset = 0){

    return new Promise( async resolve => {

      let sql = "SELECT * FROM buildings ";
      let values = [];

      if (search) {
        sql += "  where name like ? or streetAddress like ? or city like ? "
        values.push('%' + search + '%');
        values.push('%' + search + '%');
        values.push('%' + search + '%');
      }

      sql += " order by name asc limit ? OFFSET ? "
      values.push(30);
      values.push(offset);

      let d = await this.execute(sql, values);
      if (!d) {
        let obj = {
          offset: -1,
          buildings: []
        }
        resolve(obj);
        return;
      }
      // var data = d as any[];
      const data = this.getRows(d);
      if (data.length > 0) {

        offset = (data.length < 30) ? -1 : (offset + 30)

        let obj = {
          offset: offset,
          buildings: data
        }
        resolve(obj);

      } else {

        let obj = {
          offset: -1,
          buildings: []
        }
        resolve(obj);
      }

    })

  }

  public async getBuildingsItemById(id){

    return new Promise(async resolve => {
      let sql = "SELECT * FROM buildings where id = ? ";
      let values = [id];

      let d = await this.execute(sql, values);
      // var data = d as any[];
      const data = this.getRows(d);
      if (data.length > 0) {
        var user = data[0];
        resolve(user);
      } else {
        resolve(null);
      }

    })


  }


  // STOCK QUERY STARTS

  public async getStockCount(){

  }

  public async deleteAllStocks(){

    return new Promise( async resolve => {

      let sql = "DELETE FROM stocks";
      let values = [];

      let d = await this.execute(sql, values);
      resolve(d);

    })

  }

  public async setStocksInDatabase(list){

    
    return new Promise( async resolve => {

      // await this.deleteAllStocks()

      let insertRows = [];

      // get sidemenu json data from file and dump into sqlite table
      for (var i = 0; i < list.length; i++) {

        var sql = "INSERT OR REPLACE INTO stocks(";

        sql += "id, ";
        sql += "display, ";
        sql += "location, ";
        sql += "name, ";
        sql += "orderId, ";
        sql += "productCode, ";
        sql += "quantity, ";
        sql += "salesPrice, ";
        sql += "unit ";
        sql += ") ";
  
        sql += "VALUES (";
  
        sql += "?, "
        sql += "?, " 
        sql += "?, " 
        sql += "?, " 
        sql += "?, " 
        sql += "?, " 
        sql += "?, " 
        sql += "?, " 
        sql += "? " //9        
        sql += ")";

        var values = [
          list[i]["id"],
          list[i]["display"],
          list[i]["location"],
          list[i]["name"],
          list[i]["orderId"],
          list[i]["productCode"],
          list[i]["quantity"],
          list[i]["salesPrice"],
          list[i]["unit"]          
        ];
  
        // await this.execute(sql, values);
        insertRows.push([
          sql,
          values
        ]);

      }

      await this.prepareBatch(insertRows)

      resolve(true);


    })
  }

  removeStockItemById() {

    return new Promise(async resolve => {
      let sql = "DELETE FROM stocks";
      let values = [];
      await this.execute(sql, values);
      resolve(true);
    })

  }

  public async getStockItems(location, search = null, offset = 0){

    return new Promise( async resolve => {

      let sql = "SELECT * FROM stocks ";
      let values = [];

      if (search) {
        sql += "where display like ? "
        values.push('%' + search + '%');
      }

      // if(location && search){
      //   sql += " and "
      // }else{

      // }




      // if (search) {
      //   sql += " where location = ? "
      //   values.push('%' + search + '%');
      // }

      

      sql += " order by display asc limit ? OFFSET ? "
      values.push(30);
      values.push(offset);

      let d = await this.execute(sql, values);
      if (!d) {
        let obj = {
          offset: -1,
          stocks: []
        }
        resolve(obj);
        return;
      }
      // var data = d as any[];
      const data = this.getRows(d);
      if (data.length > 0) {

        offset = (data.length < 30) ? -1 : (offset + 30)

        let obj = {
          offset: offset,
          stocks: data
        }
        resolve(obj);

      } else {

        let obj = {
          offset: -1,
          stocks: []
        }
        resolve(obj);
      }

    })

  }

  // USERS QUERY STARTS

  public async setUserInDatabase(_user) {
    return new Promise(async resolve => {
      // check if user is already present in our local database, if not, create and fetch his data
      // check if user exist in database, if not create it else update it
      var sql = "INSERT OR REPLACE INTO users(";
      sql += "id, ";
      sql += "accountId, " 
      sql += "creationDate, " 
      sql += "email, " 
      sql += "firstname, " 
      sql += "industry, " 
      sql += "lastname, " 
      sql += "phoneNumber, " 
      sql += "project, " 
      sql += "refreshToken, " 
      sql += "role_id, " 
      sql += "role_name, " 
      sql += "startApi, " 
      sql += "userId, " 
      sql += "username "
      sql += ") ";

      sql += "VALUES (";

      sql += "?, "
      sql += "?, " 
      sql += "?, " 
      sql += "?, " 
      sql += "?, " 
      sql += "?, " 
      sql += "?, " 
      sql += "?, " 
      sql += "?, " 
      sql += "?, " 
      sql += "?, " 
      sql += "?, " 
      sql += "?, " 
      sql += "?, " 
      sql += "? " // 15
      sql += ")";

      var values = [
        _user.id,
        _user.accountId,
        _user.creationDate,
        _user.email,
        _user.firstname,
        _user.industry,
        _user.lastname,
        _user.phoneNumber,
        _user.project,
        _user.refreshToken,
        _user.role.id,
        _user.role.name,
        _user.startApi,
        _user.userId,
        _user.username,     ];

      await this.execute(sql, values);

      if (_user.token) {

        let sql3 = "UPDATE users SET active = ?";
        let values3 = [0];
        await this.execute(sql3, values3);

        let sql2 = "UPDATE users SET token = ?, active = ? where id = ?";
        let values2 = [_user.token, 1, _user.id];

        await this.execute(sql2, values2);

      }

      resolve(await this.getActiveUser());

    })
  }

  public async getUserSidemenu(withtoggle = false){

    let user_id = await this.getActiveUserId();
    return new Promise( async resolve => {

      // check if a user sidemenu exist ... if yes return that one ... else dump a side menu and return it 
      let sql = "SELECT * FROM sidemenu where user_id = ?";
      let values = [user_id];

      if(withtoggle){
        sql += " and toggle = ?"
        values.push(true);
      }

      sql += " order by orderpair asc"

      let d = await this.execute(sql, values);
      if (!d) {
        resolve(this.setSidemenuWithDefaultValues(user_id, withtoggle));
        return;
      }
      // var data = d as any[];
      const data = this.getRows(d);
      if (data.length > 0) {
        resolve(data);
      } else {
        resolve(this.setSidemenuWithDefaultValues(user_id, withtoggle));
      }

    })

  }

  public async setSidemenuWithDefaultValues(user_id, withtoggle = false){

    return new Promise( async resolve => {

      // get sidemenu json data from file and dump into sqlite table
      let sidemenu = menu_json;
      for (var i = 0; i < sidemenu.length; i++) {

        var sql = "INSERT OR REPLACE INTO sidemenu(";
        sql += "user_id, ";
        sql += "title, ";
        sql += "route, ";
        sql += "url, ";
        sql += "theme, ";
        sql += "icon, ";
        sql += "listView, ";
        sql += "component, ";
        sql += "singlePage, ";
        sql += "toggle, ";
        sql += "orderpair ";
        sql += ") ";
  
        sql += "VALUES (";
  
        sql += "?, "
        sql += "?, " 
        sql += "?, " 
        sql += "?, " 
        sql += "?, " 
        sql += "?, " 
        sql += "?, " 
        sql += "?, " 
        sql += "?, " 
        sql += "?, " 
        sql += "? " // 11
        sql += ")";

        var values = [
          user_id,
          sidemenu[i]["title"],
          sidemenu[i]["route"],
          sidemenu[i]["url"],
          sidemenu[i]["theme"],
          sidemenu[i]["icon"],
          sidemenu[i]["listView"],
          sidemenu[i]["component"],
          sidemenu[i]["singlePage"],
          sidemenu[i]["toggle"],
          sidemenu[i]["orderpair"],
        ];
  
        await this.execute(sql, values);

      }

      resolve(this.getSidemenuByUserId(user_id, withtoggle));


    })

  }

  public async setSidemenuItem(menu){

    return new Promise( async resolve => {

      // get sidemenu json data from file and dump into sqlite table

        var sql = "INSERT OR REPLACE INTO sidemenu(";
        sql += "user_id, ";
        sql += "title, ";
        sql += "route, ";
        sql += "url, ";
        sql += "theme, ";
        sql += "icon, ";
        sql += "listView, ";
        sql += "component, ";
        sql += "singlePage, ";
        sql += "toggle, ";
        sql += "orderpair ";
        sql += ") ";
  
        sql += "VALUES (";
  
        sql += "?, "
        sql += "?, " 
        sql += "?, " 
        sql += "?, " 
        sql += "?, " 
        sql += "?, " 
        sql += "?, " 
        sql += "?, " 
        sql += "?, " 
        sql += "?, " 
        sql += "? " // 11
        sql += ")";

        var values = [
          menu['user_id'],
          menu["title"],
          menu["route"],
          menu["url"],
          menu["theme"],
          menu["icon"],
          menu["listView"],
          menu["component"],
          menu["singlePage"],
          menu["toggle"],
          menu["orderpair"],
        ];
  
      await this.execute(sql, values);

      resolve(menu);


    })

  }

  public async getSidemenuByUserId(user_id, withtoggle = false){

    return new Promise(async resolve => {

      let sql = "SELECT * FROM sidemenu where user_id = ?";
      let values = [user_id];

      if(withtoggle){
        sql += " and toggle = ?"
        values.push(true);
      }

      sql += " order by orderpair asc"

      // let sql = "SELECT * FROM sidemenu where user_id = ? ORDER BY orderpair asc";
      // let values = [id];

      let d = await this.execute(sql, values);
      if (!d) {
        resolve(null);
      }
      // var data = d as any[];
      const data = this.getRows(d);
      if (data.length > 0) {
        resolve(data);
      } else {
        resolve(null);
      }

    })

  }


  public async getCurrentUserAuthorizationToken() {
    return new Promise(async resolve => {
      let user_id = await this.getActiveUserId();
      let sql = "SELECT token FROM users where id = ? limit 1";
      let values = [user_id];

      let d = await this.execute(sql, values);
      // this.utility.presentToast(d);
      if (!d) {
        resolve(null);
        return;
      }
      // var data = d as any[];
      const data = this.getRows(d);
      if (data.length > 0) {
        resolve(data[0]['token']);
      } else {
        resolve(null);
      }

    })
  }

  public async setUserActiveById(id) {

    return new Promise(async resolve => {

      let sql3 = "UPDATE users SET active = ?";
      let values3 = [0];
      await this.execute(sql3, values3);

      let sql2 = "UPDATE users SET active = ? where id = ?";
      let values2 = [ 1, id];
      await this.execute(sql2, values2);

      resolve(this.getUserById(id))

    })

  }

  public async setRefreshToken(token, refreshToke) {

    return new Promise(async resolve => {

      let sql3 = "UPDATE users SET token = ?, refreshToken = ? where active = ?";
      let values3 = [token, refreshToke, 1];
      await this.execute(sql3, values3);
      
      resolve(true)

    })

  }

  public async getUserById(id) {

    return new Promise(async resolve => {
      let sql = "SELECT * FROM users where id = ?";
      let values = [id];

      let d = await this.execute(sql, values);
      if (!d) {
        resolve(null);
      }
      // var data = d as any[];
      const data = this.getRows(d);
      if (data.length > 0) {
        let id = data[0];
        resolve(id);
      } else {
        resolve(null);
      }

    })

  }

  public async getActiveUserId() {

    return new Promise(async resolve => {
      let sql = "SELECT id FROM users where active = ?";
      let values = [1];

      let d = await this.execute(sql, values);
      if (!d) {
        resolve(null);
      }
      // var data = d as any[];
      const data = this.getRows(d);
      if (data.length > 0) {
        let id = data[0]["id"];
        resolve(id);
      } else {
        resolve(null);
      }

    })

  }

  public async getActiveUser() {
    return new Promise(async resolve => {
      let sql = "SELECT * FROM users where active = ? ";
      let values = [1];

      let d = await this.execute(sql, values);
      // var data = d as any[];
      const data = this.getRows(d);
      if (data.length > 0) {
        var user = data[0];
        resolve(user);
      } else {
        resolve(null);
      }

    })
  }

  setLogout() {

    return new Promise(async resolve => {
      let user_id = await this.getActiveUserId();

      let sql = "UPDATE users SET token = ?, active = ? where id = ?";
      let values = [null, 0, user_id];

      let d = await this.execute(sql, values);
      // var data = d as any[];
      const data = this.getRows(d);
      if (data.length > 0) {
        resolve(true);
      } else {
        resolve(false);
      }


    })

  }

  execute(sql, params) {
    return new Promise(async resolve => {

      if (!this.db) {
        await this.platform.ready();
        // initialize database object
        await this.createDatabase()
      }

      console.log(sql);
      // // if(this.platform.is('cordova')){
      console.log(params);
      this.db.executeSql(sql, params).then(response => {
        resolve(response);
      }).catch(err => {
        console.error(err);
        resolve(null);
      })
      
    })
  }

  prepareBatch(insertRows){

    return new Promise( async resolve => {

      var size = 250; var arrayOfArrays = [];
      
      for (var i=0; i<insertRows.length; i+=size) {
        arrayOfArrays.push(insertRows.slice(i,i+size));
      }

      console.log(arrayOfArrays);

      for(var j = 0; j < arrayOfArrays.length; j++){
        await this.executeBatch(arrayOfArrays[j]);
        // await this.execute(s, p)
      }

      resolve(true);

    })
  }

  executeBatch(array) {

    return new Promise(async resolve => {

      if (!this.db) {
        await this.platform.ready();
        // initialize database object
        await this.createDatabase()
      }

      console.log(array); 
      this.db.sqlBatch(array).then(response => {
        resolve(response);
      }).catch(err => {
        console.error(err);
        resolve(null);
      })
      
    })
  }


  private setValue(k, v) {
    return new Promise(resolve => {
      this.storage.setKey(k, v).then(() => {
        resolve({ k: v });
      });
    })

  }

  private getValue(k): Promise<any> {
    return new Promise(resolve => {
      this.storage.getKey(k).then(r => {
        resolve(r);
      });
    })

  }

  private getRows(data) {
    var items = []
    for (let i = 0; i < data.rows.length; i++) {
      let item = data.rows.item(i);

      items.push(item);
    }

    return items;
  }




}
