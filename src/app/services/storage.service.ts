import { Injectable } from '@angular/core';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { Platform } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor(private nativeStorage: NativeStorage, public platform: Platform) { 

  }

  setKey(key, value): Promise<any>{

    return new Promise(resolve => {
      // if(this.platform.is('cordova')){
      //   this.nativeStorage.setItem(key, value)
      //   .then( () => resolve(true), error => { console.error('Error storing item', error), resolve(null) });
      // }else{
        resolve(localStorage.setItem(key, JSON.stringify(value)))  
      // }
      
    })

  }

  getKey(key): Promise<any>{
    
    return new Promise(resolve => {
      // if(this.platform.is('cordova')){
      //   this.nativeStorage.getItem('myitem')
      //   .then(data => resolve(data), error => resolve(null));
      // }else{
        let v = localStorage.getItem(key);



        if (this.isJson(v)){
          v = JSON.parse(v)
        }
        resolve(v)
      // }
      
    })

  } 

  isJson(text){

    if(!text){
      return false;
    }
    if (/^[\],:{}\s]*$/.test(text.replace(/\\["\\\/bfnrtu]/g, '@').
    replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').
    replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {

      //the json is ok
      return true

    }else{

      //the json is not ok
      return false;

    }
  }

  setUserToken(token){
    this.setKey('token', token);
  }  

  async getUserToken(){
    let token = await this.getKey('token');
    return token;
  }

  setUserData(data){
    this.setKey('userdata', data);
    this.setUserToken(data['token']);
  }  


}
