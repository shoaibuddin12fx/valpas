import { TestBed } from '@angular/core/testing';

import { StoragelocationService } from './storagelocation.service';

describe('StoragelocationService', () => {
  let service: StoragelocationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(StoragelocationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
